#MUI Test Automation#
GUI automated testing framework powered by Selenium WebDriver and Cucumber JVM


#I. Installing MUI Test Automation#
Before installing the MUI test automation, there are several prerequisite that needs to be fulfilled as below
 1. Java Installed
 2. Gradle Installed
 3. Eclipse installed (optional)

#II. Creating MUI Test Automation Project#
To create new test automation project, the following steps should be followed
1. Copy the MUI Test Automation Project Folder in your local drive
2. Open command prompt and point to the previously copied MUI Test Automation Project folder
3. Type gradle build and press Enter key, this to make sure that all dependencies are downloaded. Please make sure you have internet connection
4. Create new folder and Files contains the following
  1. Pages
  2. Features
  3. properties file with extension .properties. Copy from the template provided
5. [Create your test project](./createTestProject.md "Create New Project")


#III. Running MUI Test Automation#
The following steps and configuration are needed to run the MUI Test Automation. There are 2 ways to run the test

###GRADLE###
1. Open command prompt / terminal and move to project root folder
2. type `gradle cucumber -daemon -Pproj_dir="path/to/testProject"` it will run all properties files

###Eclipse###
1. Import project to eclipse workspace
2. Run > Run configurations...
3. Click on JUnit, and add run configuration
4. On Test tab, direct Test Class to BridgeRunner
5. On Arguments tab, add VM arguments as follow :
    `-Dmitrais.configs=path/to/testProject`
    `-Dtest.configs=path/to/testProject/fileProperties`
6. save and run the test by click run

####Safari Mac only####
Safari on mac only run by grid. Because of selenium limitation, we should run another server beside selenium to run the test on safari mac  
1. Run selenium-server-standalone-2.xx.x.jar by type `java -jar selenium-server-standalone.2.xx.x.jar -role node -hub yourSeleniumGridHubAddress` on terminal  
2. After that, run robotil server by type `java -jar robotil-x.xxx.jar` on terminal

#IV. Frequently Ask Question#
Q: How to make the test more stable?  
A: You should configure the duration of the wait such as elementwait, element polling, etc. Each browser has their own javascript engine which might have different performance, so it is best to try and find the optimum value of wait for each browser and creates each properties file for each browser test.  


##Tips n Tricks##
1. Use Implicit wait for changing page
2. Set keyDomElementName on pageconfig for better wait

This is the prototype of Mitrais GUI Test Automation Framework

#Features :##
1. Behavior Driven Test
2. Data Driven Test
3. Multiple browser support (Chrome, Firefox, Safari, IE, Opera)
4. DSL test language (Gherkin format, Given, When, Then)
