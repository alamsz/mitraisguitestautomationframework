package com.mitrais.guitestautomation.testrunner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(glue = { "com.mitrais.guitestautomation.keyworddefinition" }, tags={"~@skipIE"},features = { "D:\\selenium-server-2\\crs\\features" }, plugin = {
        "pretty", 
        "json:target/cucumber-report.json" })
public class BridgeRunner {
}
