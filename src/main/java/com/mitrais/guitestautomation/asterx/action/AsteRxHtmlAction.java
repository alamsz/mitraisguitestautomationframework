package com.mitrais.guitestautomation.asterx.action;

import com.mitrais.guitestautomation.action.HtmlAction;

public enum AsteRxHtmlAction implements HtmlAction {
    SELECT("select"), FORWARD("forward"), BACKWARD("backward"), KENDO_DROPDOWN(
            "dropdown"), KENDO_EXPAND_ROW("expand"), DOUBLE_CLICK(
            "double-click"), DRAG("drag"), KENDO_DATETIME("datetime"), KENDO_CHECK(
            "check"), KENDO_DATE("date"), UPLOAD("upload"), DELETE("delete"), KENDO_CHOOSE_ITEM_GRID(
            "choose_item_grid"), KENDO_CHOOSE_ITEM_LIST("choose_item_list"), KENDO_CHOOSE_MULTISELECT_LIST(
            "choose_multiselect_list"), KENDO_DELETE_MULTISELECT_LIST(
            "delete_multiselect_list"), KENDO_ALERT_ACTION("alert_action"), KENDO_DOUBLECLICK_CELL_GRID(
            "double_click_cell_grid"), ASTERX_DRAG_APPOINTMENT(
            "asterx_drag_appointment"), ASTERX_ENTER_NUMERIC("enter_numeric"), ASTERX_CLICK_RADIO_BUTTON("click_radio_button"),
            ASTERX_CLICK_CHECK_BOX("click_check_box");

    private String actionName;

    AsteRxHtmlAction(String actionName) {
        this.actionName = actionName;
    }

    public String getActionName() {
        return actionName;
    }

    public String toString() {
        return getActionName().toUpperCase();
    }
}
