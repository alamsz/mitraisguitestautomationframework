package com.mitrais.guitestautomation.asterx.keyworddefinition;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mitrais.guitestautomation.action.GenericHtmlAction;
import com.mitrais.guitestautomation.action.HtmlActionStatus;
import com.mitrais.guitestautomation.asterx.action.AsteRxHtmlAction;
import com.mitrais.guitestautomation.asterx.processor.AsterxActionProcessor;
import com.mitrais.guitestautomation.exception.IllegalCucumberFormatException;
import com.mitrais.guitestautomation.exception.WebActionException;
import com.mitrais.guitestautomation.page.AsterxHtmlActionConditionEnum;
import com.mitrais.guitestautomation.page.DomElementExpectation;
import com.mitrais.guitestautomation.processor.ActionProcessor;
import com.mitrais.guitestautomation.processor.ActionRequest;
import com.mitrais.guitestautomation.util.Utils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MUITAsterxStepDefinition {
	    private static ActionProcessor processor = AsterxActionProcessor.getInstance();
	    private Scenario scenario;

	    @Before
	    public void before(Scenario scenario) {
	        this.scenario = scenario;
	        processor.setScenario(scenario);
	    }

	    @Given("^I have logged in using$")
	    public void loggedInUsing(DataTable arg1) throws MalformedURLException,
	            WebActionException {
	        iamOn("LoginPage");
	        enter(arg1);
	        click("loginButton");
	    }

	    @After("@last")
	    public void cleanUp() throws InterruptedException, WebActionException {
	        processor.terminateProcessor();
	    }

	    @Before("@login")
	    public void loginIn() throws WebActionException, MalformedURLException {
	        iamOn("LoginPage");
	        Map<String, String> dataEntry = new HashMap<String, String>();
	        dataEntry.put("username", "olivia.charlotte");
	        dataEntry.put("email", "oliviacharlotte.doctor@gmail.com");
	        dataEntry.put("password", "password");
	        enterAction(dataEntry);
	        click("loginButton");
	    }

	    @Given("^I am ON \"(.*?)\"$")
	    public void iamOn(String arg1) throws WebActionException {
	        processor.setup(arg1);
	        assertTrue("no exception on setup", true);
	    }

	    @Then("^I should EXPECT$")
	    public void shouldExpect(DataTable data) throws WebActionException,
	            IllegalCucumberFormatException {
	        for (DomElementExpectation elementExpectation : Utils.expectBridge(data)) {
	            assertTrue(processor.doExpect(elementExpectation));
	        }
	    }

	    @Then("^I expect \"(.*?)\" dropdown list content$")
	    public void expectDropdownListContent(String identifier, DataTable data)
	            throws WebActionException, IllegalCucumberFormatException {
	        DomElementExpectation expectation = new DomElementExpectation();
	        expectation.setComponentName(identifier);
	        expectation.setValue(data.asList(String.class));
	        expectation
	                .setCondition(AsterxHtmlActionConditionEnum.MEMBER_DROPDOWN_CONTENT
	                        .getValue());
	        assertTrue(processor.doExpect(expectation));
	    }

	    @Then("^I expect \"(.*?)\" dropdown list contains$")
	    public void expectDropdownListContain(String identifier, DataTable data)
	            throws WebActionException, IllegalCucumberFormatException {
	        DomElementExpectation expectation = new DomElementExpectation();
	        expectation.setComponentName(identifier);
	        expectation.setValue(data.asList(String.class));
	        expectation
	                .setCondition(AsterxHtmlActionConditionEnum.MEMBER_DROPDOWN_CONTAIN
	                        .getValue());
	        assertTrue(processor.doExpect(expectation));
	    }

	    @Then("^I expect \"(.*?)\" multi select grid contains$")
	    public void expectGridContain(String identifier, DataTable data)
	            throws WebActionException, IllegalCucumberFormatException {
	        DomElementExpectation expectation = new DomElementExpectation();
	        expectation.setComponentName(identifier);
	        expectation.setValue(data.asList(String.class));
	        expectation
	                .setCondition(AsterxHtmlActionConditionEnum.MEMBER_MULTISELECT_GRID_CONTAIN
	                        .getValue());
	        assertTrue(processor.doExpect(expectation));
	    }

	    @Then("^I expect \"(.*?)\" multi select grid content$")
	    public void expectGridContent(String identifier, DataTable data)
	            throws WebActionException, IllegalCucumberFormatException {
	        DomElementExpectation expectation = new DomElementExpectation();
	        expectation.setComponentName(identifier);
	        expectation.setValue(data.asList(String.class));
	        expectation
	                .setCondition(AsterxHtmlActionConditionEnum.MEMBER_MULTISELECT_GRID_CONTENT
	                        .getValue());
	        assertTrue(processor.doExpect(expectation));
	    }

	    @Then("^I expect \"(.*?)\" multi select list contains$")
	    public void expectListContain(String identifier, DataTable data)
	            throws WebActionException, IllegalCucumberFormatException {
	        DomElementExpectation expectation = new DomElementExpectation();
	        expectation.setComponentName(identifier);
	        expectation.setValue(data.asList(String.class));
	        expectation
	                .setCondition(AsterxHtmlActionConditionEnum.MEMBER_MULTISELECT_LIST_CONTAIN
	                        .getValue());
	        assertTrue(processor.doExpect(expectation));
	    }

	    @Then("^I expect \"(.*?)\" multi select list content$")
	    public void expectListContent(String identifier, DataTable data)
	            throws WebActionException, IllegalCucumberFormatException {
	        DomElementExpectation expectation = new DomElementExpectation();
	        expectation.setComponentName(identifier);
	        expectation.setValue(data.asList(String.class));
	        expectation
	                .setCondition(AsterxHtmlActionConditionEnum.MEMBER_MULTISELECT_LIST_CONTENT
	                        .getValue());
	        assertTrue(processor.doExpect(expectation));
	    }

	    @Then("^I expect \"(.*?)\" multi select item selected content$")
	    public void expectMultiselectItemSelectedContent(String identifier,
	            DataTable data) throws WebActionException,
	            IllegalCucumberFormatException {
	        DomElementExpectation expectation = new DomElementExpectation();
	        expectation.setComponentName(identifier);
	        expectation.setValue(data.asList(String.class));
	        expectation
	                .setCondition(AsterxHtmlActionConditionEnum.MEMBER_MULTISELECT_SELECTED_CONTENT
	                        .getValue());
	        assertTrue(processor.doExpect(expectation));
	    }

	    @Then("^I expect \"(.*?)\" multi select item selected contains$")
	    public void expectMultiselectItemSelectedContains(String identifier,
	            DataTable data) throws WebActionException,
	            IllegalCucumberFormatException {
	        DomElementExpectation expectation = new DomElementExpectation();
	        expectation.setComponentName(identifier);
	        expectation.setValue(data.asList(String.class));
	        expectation
	                .setCondition(AsterxHtmlActionConditionEnum.MEMBER_MULTISELECT_SELECTED_CONTAINS
	                        .getValue());
	        assertTrue(processor.doExpect(expectation));
	    }

	    @Then("^I expect \"(.*?)\" multi select item options contains$")
	    public void expectMultiselectItemOptionsContains(String identifier,
	            DataTable data) throws WebActionException,
	            IllegalCucumberFormatException {
	        DomElementExpectation expectation = new DomElementExpectation();
	        expectation.setComponentName(identifier);
	        expectation.setValue(data.asList(String.class));
	        expectation
	                .setCondition(AsterxHtmlActionConditionEnum.MEMBER_MULTISELECT_OPTION_CONTAINS
	                        .getValue());
	        assertTrue(processor.doExpect(expectation));
	    }

	    @Then("^I expect \"(.*?)\" multi select item options content$")
	    public void expectMultiselectItemOptionsContent(String identifier,
	            DataTable data) throws WebActionException,
	            IllegalCucumberFormatException {
	        DomElementExpectation expectation = new DomElementExpectation();
	        expectation.setComponentName(identifier);
	        expectation.setValue(data.asList(String.class));
	        expectation
	                .setCondition(AsterxHtmlActionConditionEnum.MEMBER_MULTISELECT_OPTION_CONTENT
	                        .getValue());
	        assertTrue(processor.doExpect(expectation));
	    }

	    @Then("^I expect \"(.*?)\" is on time \"(.*?)\" with provider \"(.*?)\"$")
	    public void expectAppointmentOn(String appointmentIdentifier, String time,
	            String doctor) throws WebActionException {
	        DomElementExpectation expectation = new DomElementExpectation();
	        expectation.setComponentName(Utils.normalizeIdentifier(appointmentIdentifier));
	        List<String> parameters = new ArrayList<>();
	        parameters.add(time);
	        parameters.add(doctor);
	        expectation.setValue(parameters);
	        expectation
	                .setCondition(AsterxHtmlActionConditionEnum.APPOINTMENT_LOCATION_VALIDATION
	                        .getValue());
	        assertTrue(processor.doExpect(expectation));
	    }

	    @Given("^I have hovered \"(.*?)\"$")
	    @When("^I hover \"(.*?)\"$")
	    public void i_hover(String identifier) throws Throwable {
	        ActionRequest request = new ActionRequest(GenericHtmlAction.HOVER, identifier,
	                scenario.getName() + " Hover");
	        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	    }

	    @Given("^I have clicked \"(.*?)\"$")
	    @When("^I click \"(.*?)\"$")
	    public void click(String identifier) throws WebActionException {
	        ActionRequest request = new ActionRequest(GenericHtmlAction.CLICK,
	                Utils.normalizeIdentifier(identifier), scenario.getName()
	                        + " Click");
	        HtmlActionStatus status = processor.doAction(request);
	        assertEquals(HtmlActionStatus.GOOD, status);
	    }

	    @Given("^I have clicked radio button \"(.*?)\"$")
	    @When("^I click radio button \"(.*?)\"$")
	    public void clickRadioButton(String identifier) throws WebActionException {
	        ActionRequest request = new ActionRequest(
	                AsteRxHtmlAction.ASTERX_CLICK_RADIO_BUTTON,
	                Utils.normalizeIdentifier(identifier), scenario.getName()
	                        + " Click Radio Button");
	        HtmlActionStatus status = processor.doAction(request);
	        assertEquals(HtmlActionStatus.GOOD, status);
	    }

	    @Given("^I have clicked check box \"(.*?)\"$")
	    @When("^I click check box \"(.*?)\"$")
	    public void clickCheckBox(String identifier) throws WebActionException {
	        ActionRequest request = new ActionRequest(
	                AsteRxHtmlAction.ASTERX_CLICK_CHECK_BOX,
	                Utils.normalizeIdentifier(identifier), scenario.getName()
	                        + " Click Checkbox");
	        HtmlActionStatus status = processor.doAction(request);
	        assertEquals(HtmlActionStatus.GOOD, status);
	    }

	    @Given("^I have clicked \"(.*?)\" if \"(.*?)\" is present$")
	    @When("^I click \"(.*?)\" if \"(.*?)\" is present$")
	    public void clickAlert(String alertButtonLabel, String alertIdentifier)
	            throws WebActionException {
	        ActionRequest request = new ActionRequest(
	                AsteRxHtmlAction.KENDO_ALERT_ACTION,
	                Utils.normalizeIdentifier(alertIdentifier),
	                scenario.getName() + " Click if present");
	        request.addParameters(alertButtonLabel);
	        HtmlActionStatus status = processor.doAction(request);
	        assertEquals(HtmlActionStatus.GOOD, status);
	    }

	    @Given("^I have entered$")
	    @When("^I enter$")
	    public void enter(DataTable data) throws WebActionException {
	        Map<String, String> dataEntry = data.asMap(String.class, String.class);
	        enterAction(dataEntry);
	    }

	    @Given("^I have entered numeric$")
	    @When("^I enter numeric$")
	    public void enterNumeric(DataTable data) throws WebActionException {
	        Map<String, Integer> dataEntry = data
	                .asMap(String.class, Integer.class);
	        enterNumericAction(dataEntry);
	    }

	    @Given("^I have entered \"(.*?)\" in \"(.*?)\"$")
	    @When("^I enter \"(.*?)\" in \"(.*?)\"$")
	    public void enterIn(String content, String elementIdentifier)
	            throws WebActionException {
	        Map<String, String> dataEntry = new HashMap<>();
	        dataEntry.put(elementIdentifier, content);
	        enterAction(dataEntry);
	    }

	    @Given("^I have entered (\\d+) in \"(.*?)\"$")
	    @When("^I enter (\\d+) in \"(.*?)\"$")
	    public void enterNumericIn(int content, String elementIdentifier)
	            throws WebActionException {
	        Map<String, Integer> dataEntry = new HashMap<>();
	        dataEntry.put(elementIdentifier, content);
	        enterNumericAction(dataEntry);
	    }

	    @Given("^I have double-clicked on \"(.*?)\" with time \"(.*?)\" with doctor \"(.*?)\"$")
	    @When("^I double-click on \"(.*?)\" with time \"(.*?)\" with doctor \"(.*?)\"$")
	    public void doubleClickOn(String gridIdentifier, String time, String doctor)
	            throws WebActionException {
	        ActionRequest request = new ActionRequest(
	                AsteRxHtmlAction.KENDO_DOUBLECLICK_CELL_GRID,
	                Utils.normalizeIdentifier(gridIdentifier),
	                scenario.getName() + " Double click time doctor");
	        request.addParameters(time);
	        request.addParameters(doctor);
	        HtmlActionStatus status = processor.doAction(request);
	        assertEquals(HtmlActionStatus.GOOD, status);
	    }

	    private void enterAction(Map<String, String> dataEntry)
	            throws WebActionException {
	        for (String identifier : dataEntry.keySet()) {
	            ActionRequest request = new ActionRequest(GenericHtmlAction.ENTER,
	                    Utils.normalizeIdentifier(identifier),
	                    scenario.getName() + " Enter");
	            request.addParameters(dataEntry.get(identifier));
	            HtmlActionStatus status = processor.doAction(request);
	            assertEquals(HtmlActionStatus.GOOD, status);
	        }
	    }

	    private void enterNumericAction(Map<String, Integer> dataEntry)
	            throws WebActionException {
	        for (String identifier : dataEntry.keySet()) {
	            ActionRequest request = new ActionRequest(
	                    AsteRxHtmlAction.ASTERX_ENTER_NUMERIC,
	                    Utils.normalizeIdentifier(identifier),
	                    scenario.getName() + " Enter Numeric");
	            request.addParameters(dataEntry.get(identifier));
	            HtmlActionStatus status = processor.doAction(request);
	            assertEquals(HtmlActionStatus.GOOD, status);
	        }
	    }

	    @Then("^I should be ON \"(.*?)\"$")
	    public void shouldBeOn(String arg1) throws WebActionException,
	            MalformedURLException {
	        processor.setup(arg1);
	        assertTrue("no exception on setup", true);
	    }

	    @Given("^I have selected$")
	    @When("^I select$")
	    public void select(DataTable data) throws WebActionException {
	        Map<String, String> dataEntry = data.asMap(String.class, String.class);
	        for (String identifier : dataEntry.keySet()) {
	            ActionRequest request = new ActionRequest(AsteRxHtmlAction.SELECT,
	                    Utils.normalizeIdentifier(identifier),
	                    scenario.getName() + " Select");
	            request.addParameters(dataEntry.get(identifier));
	            assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	        }
	    }

	    @Given("^I have waited (\\d+) seconds$")
	    @When("^I wait (\\d+) seconds$")
	    public void waitSeconds(int arg1) throws WebActionException {
	        ActionRequest request = new ActionRequest(GenericHtmlAction.WAIT, null,
	                scenario.getName() + " wait");
	        request.addParameters(arg1);
	        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	    }

	    @Given("^I have selected dropdown$")
	    @When("^I select dropdown$")
	    public void selectDropdown(DataTable data) throws WebActionException {
	        Map<String, String> dataEntry = data.asMap(String.class, String.class);
	        for (String identifier : dataEntry.keySet()) {
	            ActionRequest request = new ActionRequest(
	                    AsteRxHtmlAction.KENDO_DROPDOWN,
	                    Utils.normalizeIdentifier(identifier),
	                    scenario.getName() + " Select Dropdown");
	            request.addParameters(dataEntry.get(identifier));
	            assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	        }
	    }

	    /**
	     * Expand one of row from grid
	     * 
	     * @param rowIdentifier
	     *            identifier of row. Can be one of value from row
	     * @param gridIdentifier
	     *            the grid that contain selected row
	     * @throws WebActionException
	     *             if row doesn't have expand button (not expandable)
	     */
	    @Given("^I have expanded \"(.*?)\" at \"(.*?)\"$")
	    @When("^I expand \"(.*?)\" at \"(.*?)\"$")
	    public void expandAt(String rowIdentifier, String gridIdentifier)
	            throws WebActionException {
	        ActionRequest request = new ActionRequest(AsteRxHtmlAction.KENDO_EXPAND_ROW,
	                gridIdentifier, "Expand");
	        request.addParameters(rowIdentifier);
	        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	    }

	    @Given("^I have refreshed page$")
	    @When("^I refresh page$")
	    public void refreshPage() throws WebActionException {
	        ActionRequest request = new ActionRequest(GenericHtmlAction.REFRESH, null,
	                scenario.getName() + " Refresh page");
	        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	    }

	    @Given("^I have dragged \"(.*?)\" to \"(.*?)\"$")
	    @When("^I drag \"(.*?)\" to \"(.*?)\"$")
	    public void dragTo(String origin, String destination)
	            throws WebActionException {
	        ActionRequest request = new ActionRequest(AsteRxHtmlAction.DRAG,
	                Utils.normalizeIdentifier(origin), "Drag");
	        request.addParameters(Utils.normalizeIdentifier(destination));
	        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	    }

	    @Given("^I have dragged \"(.*?)\" to time \"(.*?)\" with provider \"(.*?)\"$")
	    @When("^I drag \"(.*?)\" to time \"(.*?)\" with provider \"(.*?)\"$")
	    public void asterxDragTo(String origin, String time, String doctor)
	            throws WebActionException {
	        ActionRequest request = new ActionRequest(
	                AsteRxHtmlAction.ASTERX_DRAG_APPOINTMENT,
	                Utils.normalizeIdentifier(origin), scenario.getName()
	                        + " Drag time with provider");
	        request.addParameters(time);
	        request.addParameters(doctor);
	        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	    }

	    @Given("^I have double-clicked ON \"(.*?)\"$")
	    @When("^I double-click ON \"(.*?)\"$")
	    public void doubleClickOn(String identifier) throws WebActionException {
	        ActionRequest request = new ActionRequest(GenericHtmlAction.DOUBLE_CLICK,
	                Utils.normalizeIdentifier(identifier), scenario.getName()
	                        + " Double click on");
	        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	    }

	    @Given("^I have picked datetime$")
	    @When("^I pick datetime$")
	    public void pickDateTime(DataTable arg1) throws WebActionException {
	        Map<String, String> dataTable = arg1.asMap(String.class, String.class);
	        for (String key : dataTable.keySet()) {
	            ActionRequest request = new ActionRequest(
	                    AsteRxHtmlAction.KENDO_DATETIME,
	                    Utils.normalizeIdentifier(key), scenario.getName()
	                            + " pick datetime");
	            request.addParameters(dataTable.get(key));
	            assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	        }
	    }

	    @Given("^I have deleted \"(.*?)\"$")
	    @When("^I delete \"(.*?)\"$")
	    public void delete(String identifier) throws WebActionException {
	        ActionRequest request = new ActionRequest(AsteRxHtmlAction.DELETE,
	                Utils.normalizeIdentifier(identifier), scenario.getName()
	                        + " delete");
	        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	    }

	    @Given("^I have picked date$")
	    @When("^I pick date$")
	    public void pickDate(DataTable arg1) throws WebActionException {
	        Map<String, String> dataTable = arg1.asMap(String.class, String.class);
	        for (String key : dataTable.keySet()) {
	            ActionRequest request = new ActionRequest(AsteRxHtmlAction.KENDO_DATE,
	                    Utils.normalizeIdentifier(key), scenario.getName()
	                            + " pick date");
	            request.addParameters(dataTable.get(key));
	            assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	        }
	    }

	    @Given("^I have uploaded$")
	    @When("^I upload$")
	    public void upload(DataTable arg1) throws WebActionException {
	        Map<String, String> dataTable = arg1.asMap(String.class, String.class);
	        for (String key : dataTable.keySet()) {
	            ActionRequest request = new ActionRequest(AsteRxHtmlAction.UPLOAD,
	                    Utils.normalizeIdentifier(key), scenario.getName()
	                            + " upload");
	            request.addParameters(dataTable.get(key));
	            assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	        }
	    }

	    @Given("^I have checked \"(.*?)\" at \"(.*?)\"$")
	    @When("^I check \"(.*?)\" at \"(.*?)\"$")
	    public void checkAt(String arg1, String arg2) throws WebActionException {
	        ActionRequest request = new ActionRequest(AsteRxHtmlAction.KENDO_CHECK,
	                Utils.normalizeIdentifier(arg2), scenario.getName()
	                        + " check");
	        request.addParameters(arg1);
	        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	    }

	    @Given("^I have choose item from \"(.*?)\" grid$")
	    @When("^I choose item from \"(.*?)\" grid$")
	    public void chooseItemGrid(String arg1, DataTable arg2)
	            throws WebActionException {
	        ActionRequest request = new ActionRequest(
	                AsteRxHtmlAction.KENDO_CHOOSE_ITEM_GRID,
	                Utils.normalizeIdentifier(arg1), scenario.getName()
	                        + " choose item from grid");
	        List<String> items = arg2.asList(String.class);
	        for (String item : items) {
	            request.addParameters(item);
	        }
	        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	    }

	    @Given("^I have choose item from \"(.*?)\" listbox$")
	    @When("^I choose item from \"(.*?)\" listbox$")
	    public void chooseItemListbox(String arg1, DataTable arg2)
	            throws WebActionException {
	        ActionRequest request = new ActionRequest(
	                AsteRxHtmlAction.KENDO_CHOOSE_ITEM_LIST,
	                Utils.normalizeIdentifier(arg1), scenario.getName()
	                        + " choose item from listbox");
	        List<String> items = arg2.asList(String.class);
	        for (String item : items) {
	            request.addParameters(item);
	        }
	        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	    }

	    @Given("^I have choose items from multi select dropdown \"(.*?)\"$")
	    @When("^I choose items from multi select dropdown \"(.*?)\"$")
	    public void chooseMultiselectListbox(String arg1, DataTable arg2)
	            throws WebActionException {
	        ActionRequest request = new ActionRequest(
	                AsteRxHtmlAction.KENDO_CHOOSE_MULTISELECT_LIST,
	                Utils.normalizeIdentifier(arg1), scenario.getName()
	                        + " choose items from multi select");
	        List<String> items = arg2.asList(String.class);
	        for (String item : items) {
	            request.addParameters(item);
	        }
	        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	    }

	    @Given("^I have deleted items from multi select dropdown \"(.*?)\"$")
	    @When("^I delete items from multi select dropdown \"(.*?)\"$")
	    public void deleteMultiselectListbox(String arg1, DataTable arg2)
	            throws WebActionException {
	        ActionRequest request = new ActionRequest(
	                AsteRxHtmlAction.KENDO_DELETE_MULTISELECT_LIST,
	                Utils.normalizeIdentifier(arg1), scenario.getName()
	                        + " deleted items from multi select");
	        List<String> items = arg2.asList(String.class);
	        for (String item : items) {
	            request.addParameters(item);
	        }
	        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	    }
	    
	    

	    @Given("^I have choose file \"(.*?)\" in upload \"(.*?)\" and \"(.*?)\"$")
	    @When("^I choose file \"(.*?)\" in upload \"(.*?)\" and \"(.*?)\"$")
	    public void chooseFile(String filePath, String fieldId, String buttonId)
	            throws WebActionException {
	        ActionRequest request = new ActionRequest(GenericHtmlAction.CHOOSE_FILE, null,
	                scenario.getName() + " check");
	        request.addParameters(filePath);
	        request.addParameters(fieldId);
	        request.addParameters(buttonId);
	        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
	    }
	   
	    
}
