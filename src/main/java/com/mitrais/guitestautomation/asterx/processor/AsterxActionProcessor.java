package com.mitrais.guitestautomation.asterx.processor;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mitrais.guitestautomation.action.GenericHtmlAction;
import com.mitrais.guitestautomation.action.HtmlAction;
import com.mitrais.guitestautomation.action.HtmlActionStatus;
import com.mitrais.guitestautomation.asterx.action.AsteRxHtmlAction;
import com.mitrais.guitestautomation.asterx.model.SchedulerCell;
import com.mitrais.guitestautomation.controller.WebUIManager;
import com.mitrais.guitestautomation.exception.WebActionException;
import com.mitrais.guitestautomation.page.AsterxHtmlActionConditionEnum;
import com.mitrais.guitestautomation.page.DomElement;
import com.mitrais.guitestautomation.page.DomElementExpectation;
import com.mitrais.guitestautomation.page.GenericHtmlActionConditionEnum;
import com.mitrais.guitestautomation.page.HtmlActionCondition;
import com.mitrais.guitestautomation.processor.ActionRequest;
import com.mitrais.guitestautomation.processor.GenericActionProcessor;
import com.mitrais.guitestautomation.util.Browsers;
import com.mitrais.guitestautomation.util.Constants;

public class AsterxActionProcessor extends GenericActionProcessor {

    private static final String XPATH_SEARCH_BY_TEXT = "descendant::*[normalize-space(text())='%s']";
    private static final String XPATH_KENDO_ROW = "descendant::tr[contains(@class,'k-master-row')]/";
    private static final String XPATH_APPOINTMENT_SCHEDULER_TIME = "descendant::*[@class='k-scheduler-times']/descendant::*[@class='k-slot-cell']";
    private static final String XPATH_APPOINTMENT_DOCTORS = "descendant::*[@class='k-scheduler-header-wrap']/descendant::*[@class='k-slot-cell']";
    private static final String XPATH_APPOINTMENT_CELL = "descendant::tr[@class = 'k-middle-row'][%d]/td[@class='k-today'][%d]";
    private static final String FIRST_TD = "td[1]";

    private static final Logger LOG = LoggerFactory
            .getLogger(AsterxActionProcessor.class);

    private static AsterxActionProcessor mInstance;

    public static synchronized AsterxActionProcessor getInstance() {
        if (mInstance == null) {
            mInstance = new AsterxActionProcessor();
        }
        return mInstance;
    }

    @Override
    protected HtmlActionStatus customAction(ActionRequest actionRequest)
            throws WebActionException {
        boolean customActionException = true;
        AsteRxHtmlAction action = (AsteRxHtmlAction) actionRequest.getAction();
        try {
            switch (action) {
            case KENDO_ALERT_ACTION:
                handleKendoAlertAction(actionRequest.getElementIdentifier(),
                        (String) actionRequest.getParameter(0));
                break;
            default:
                customActionException = false;
                // Element action
                WebElement element = populateElement(actionRequest
                        .getElementIdentifier());
                return handleAction(element, action,
                        actionRequest.getParameters());
            }
            return HtmlActionStatus.GOOD;
        } catch (WebActionException e) {
            if (customActionException) {
                LOG.error("Conditional Action [" + action + "] failed", e);
                throw new WebActionException("Conditional Action [" + action
                        + "] failed");
            } else {
                throw e;
            }
        }
    }

    private String ACTION_FAIL_FORMAT = "Action %s Failed";

    @Override
    protected HtmlActionStatus handleAction(WebElement element,
            HtmlAction action, List<Object> params) throws WebActionException {
        try {

            if (action instanceof GenericHtmlAction) {
                super.handleAction(element, action, params);
            } else {
                switch ((AsteRxHtmlAction) action) {
                case KENDO_DROPDOWN:
                    handleKendoDropdownAction(element, (String) params.get(0));
                    break;
                case KENDO_EXPAND_ROW:
                    handleKendoExpandRowAction(element, (String) params.get(0));
                    break;
                case KENDO_DATETIME:
                    handleKendoDateTimeAction(element, (String) params.get(0));
                    break;
                case KENDO_DATE:
                    handleKendoDateAction(element, (String) params.get(0));
                    break;
                case DELETE:
                    handleKendoDeleteAction(element);
                    break;
                case KENDO_CHECK:
                    handleKendoCheckAction(element, (String) params.get(0));
                    break;
                case KENDO_CHOOSE_ITEM_GRID:
                    handleKendoChooseItemGridAction(element, params);
                    break;
                case KENDO_CHOOSE_ITEM_LIST:
                    handleKendoChooseItemListAction(element, params);
                    break;
                case KENDO_CHOOSE_MULTISELECT_LIST:
                    handleKendoMultiselectList(element, params);
                    break;
                case KENDO_DELETE_MULTISELECT_LIST:
                    handleKendoMultiselectDeleteList(element, params);
                    break;
                case KENDO_DOUBLECLICK_CELL_GRID:
                    handleDoubleClickCellAction(element,
                            (String) params.get(0), (String) params.get(1));
                    break;
                case ASTERX_DRAG_APPOINTMENT:
                    handleAsterxDragAppointment(element,
                            (String) params.get(0), (String) params.get(1));
                    break;
                case ASTERX_ENTER_NUMERIC:
                    handleAsterxEnterNumeric(element, (int) params.get(0));
                    break;
                case ASTERX_CLICK_RADIO_BUTTON:
                    handleAsterxRadioButton(element);
                    break;
                case ASTERX_CLICK_CHECK_BOX:
                    handleAsterxCheckbox(element);
                    break;
                default:
                    break;
                }
            }

            return HtmlActionStatus.GOOD;
        } catch (WebActionException e) {
            LOG.error(String.format(ACTION_FAIL_FORMAT,
                    ((AsteRxHtmlAction) action).name()), e);
            throw new WebActionException(String.format(ACTION_FAIL_FORMAT,
                    ((AsteRxHtmlAction) action).name()));
        } catch (ParseException e) {
            LOG.error("Parse error", e);
            throw new WebActionException(String.format(ACTION_FAIL_FORMAT,
                    ((AsteRxHtmlAction) action).name()));
        } catch (MalformedURLException e) {
            LOG.error("Malformed URL", e);
            throw new WebActionException(String.format(ACTION_FAIL_FORMAT,
                    ((AsteRxHtmlAction) action).name()));
        }
    }

    @Override
    protected void customScroll(WebElement element) {
        try {
            Actions dragAction = new Actions(testManager.getWebDriver());
            int maxTry = 2;
            int attempt = 0;
            WebElement container = populateElement(
                    element,
                    By.xpath("ancestor::*[contains(@class,'mCustomScrollBox')]/descendant::*[@class='mCSB_draggerRail']"));
            WebElement dragger = populateElement(
                    element,
                    By.xpath("ancestor::*[contains(@class,'mCustomScrollBox')]/descendant::*[@class='mCSB_dragger_bar']"));
            LOG.info("starting scroll : " + container);
            int posX = 0;
            int posY = 0;
            if (testManager.getBrowser().equals(Browsers.Safari)) {
                posX = container.getLocation().x + container.getSize().width
                        / 2;
                posY = container.getLocation().y + container.getSize().height
                        - 1;
            } else {
                posX = container.getSize().width / 2;
                posY = container.getSize().height - 2;
                dragAction.moveToElement(container, posX, posY).build()
                        .perform();
            }
            int lastY = dragger.getLocation().y;
            boolean moveUp = false;
            while (!super.isVisible(element) && attempt < maxTry) {
                if (testManager.getBrowser().equals(Browsers.Safari)) {
                    testManager
                            .getJQueryDriver()
                            .executeScript(
                                    "var e = $.Event('click'); e.pageX = arguments[0]; e.pageY = arguments[1]; $(arguments[2]).trigger(e)",
                                    posX, posY, container);
                } else {
                    dragAction.click().build().perform();
                }
                handleWaitAction(1000);
                if (super.isVisible(element)) {
                    break;
                }

                boolean rightDirection = (moveUp) ? dragger.getLocation().y <= lastY
                        : dragger.getLocation().y >= lastY;
                if (!rightDirection) {
                    LOG.error("Element scroll error.");
                    break;
                }

                if (lastY == dragger.getLocation().y) {
                    attempt++;
                    if (attempt == maxTry && !moveUp) {
                        attempt = 0;
                        moveUp = true;
                        if (testManager.getBrowser().equals(Browsers.Safari)) {
                            posX = container.getLocation().x
                                    + container.getSize().width / 2;
                            posY = container.getLocation().y + 1;
                        } else {
                            posY = 1;
                            dragAction.moveToElement(container, posX, posY)
                                    .build().perform();
                        }
                    }
                }
                lastY = dragger.getLocation().y;
            }
        } catch (WebDriverException e) {
            LOG.error("Custom Scrolling not supported");
        }
        if (!super.isVisible(element)) {
            LOG.error("fail scroll");
        }
    }

    protected void handleUploadAction(WebElement element, String pathToFile)
            throws WebActionException {
        WebElement imageLayout = populateElement(element, By.xpath(".."));
        handleHoverAction(imageLayout);
        WebElement uploadButton = populateElement(imageLayout,
                By.xpath("descendant::*[@class='mr-photo-info']"));
        handleClickAction(uploadButton);
        handleWaitAction(WebUIManager.getElementPoolingTimeout());
        if (!testManager.getBrowser().equals(Browsers.Safari)) {
            testManager.getAutoitDriver().switchTo()
                    .window(getUploadDialogTitle(testManager.getBrowser()));
            testManager.getAutoitDriver().findElement(By.className("Edit1"))
                    .sendKeys(pathToFile);
            testManager.getAutoitDriver().findElement(By.className("Button1"))
                    .click();
        } else {
            safariUpload(pathToFile);
        }
        handleHoverAction(imageLayout);
        WebElement saveButton = populateElement(By
                .xpath("//*[@id='scr-pat-layout-01-patient-upload-picture' and text() = 'Save']"));
        handleClickAction(saveButton);
    }

    /**
     * Method to handle conditional alert. If alert is present, then click given
     * string label.
     * 
     * @param elementIdentifier
     * @param params
     * @throws WebActionException
     */
    private void handleKendoAlertAction(String elementIdentifier, String params)
            throws WebActionException {
        // accepting alert in page.
        List<WebElement> elementsQuery = populateElements(elementIdentifier);
        if (!elementsQuery.isEmpty()) {
            WebElement alertContainer = elementsQuery.get(0);
            if (alertContainer.isDisplayed()) {
                WebElement alertButton = populateElement(alertContainer,
                        By.xpath(String.format(XPATH_SEARCH_BY_TEXT, params)));
                handleClickAction(alertButton);
            }
        }
    }

    private void handleAsterxDragAppointment(WebElement element, String time,
            String doctor) throws WebActionException {
        WebElement grid = populateElement(element,
                By.xpath("ancestor::*[@data-role='scheduler']"));
        WebElement targetLocation = getSchedulerCell(grid, time, doctor);
        handleDragAction(element, targetLocation);
    }

    private void handleKendoMultiselectList(WebElement element,
            List<Object> params) throws WebActionException {
        WebElement inputMultiSelect = populateElement(
                element,
                By.xpath("preceding-sibling::*[contains(@class,'k-multiselect-wrap')]/input"));
        String[] owns = inputMultiSelect.getAttribute("aria-owns").split(" ");
        WebElement handlerMultiSelect = populateElement(
                element,
                By.xpath("preceding-sibling::*[contains(@class,'k-multiselect-wrap')]"));
        // TODO Re-check for another use of multiselect
        String idSelectedOption = owns[0];
        String idAvailableOption = owns[1];

        // clean up selected value
        for (WebElement current : populateElements(By.xpath("//ul[@id='"
                + idSelectedOption + "']/li"))) {
            WebElement deleteButton = populateElement(current,
                    By.xpath("descendant::*[contains(@class,'k-delete')]"));
            handleClickAction(deleteButton);
        }

        for (String selected : (List<String>) (List<?>) params) {
            handleClickAction(handlerMultiSelect);
            WebElement targetOptions = populateElement(By.xpath("//ul[@id='"
                    + idAvailableOption + "']/li[normalize-space(text())='"
                    + selected + "']"));
            try {
                populateElements(null,
                        By.xpath("//ul[@id='" + idAvailableOption + "']/li"),
                        true, WebUIManager.getElementContentTimeout());
            } catch (TimeoutException e) {
                LOG.debug("Timeout exception - Scroll detected", e);
            }
            handleClickAction(targetOptions);
            try {
                Thread.sleep(WebUIManager.getElementPoolingTimeout());
            } catch (InterruptedException e) {
                throw new WebActionException("Interrupted");
            }
        }

        List<WebElement> selectedOptions = populateElements(By
                .xpath("//ul[@id='" + idSelectedOption + "']/li"));
        if (selectedOptions.size() != params.size()) {
            throw new WebActionException("Not all value selected");
        }
    }

    private void handleKendoMultiselectDeleteList(WebElement element,
            List<Object> params) throws WebActionException {
        WebElement inputMultiSelect = populateElement(
                element,
                By.xpath("preceding-sibling::*[contains(@class,'k-multiselect-wrap')]/input"));
        String[] owns = inputMultiSelect.getAttribute("aria-owns").split(" ");
        WebElement handlerMultiSelect = populateElement(
                element,
                By.xpath("preceding-sibling::*[contains(@class,'k-multiselect-wrap')]"));
        // TODO Re-check for another use of multiselect
        String idSelectedOption = owns[0];
        String idAvailableOption = owns[1];
        List<String> items = new ArrayList<>((List<String>) (List<?>) params);

        // clean up selected value
        for (WebElement current : populateElements(By.xpath("//ul[@id='"
                + idSelectedOption + "']/li"))) {
            WebElement currentValue = populateElement(current,
                    By.xpath("descendant::span[1]"));
            if (params.contains(currentValue.getText())) {
                items.remove(currentValue.getText());
                WebElement deleteButton = populateElement(current,
                        By.xpath("descendant::*[contains(@class,'k-delete')]"));
                handleClickAction(deleteButton);
            }
        }
        if (!items.isEmpty()) {
            throw new WebActionException("Not all value deleted");
        }
    }

    /**
     * Handle multiple select on Grid AsteRx Project
     * 
     * @param element
     * @param choosenItem
     * @throws WebActionException
     */
    private void handleKendoChooseItemGridAction(WebElement element,
            List<Object> choosenItem) throws WebActionException {
        List<WebElement> options = populateElements(
                element,
                By.xpath("preceding-sibling::*[@data-role='grid']/descendant::tr[@data-uid]"));
        List<WebElement> selectedOptions = populateElements(
                element,
                By.xpath("preceding-sibling::*[@data-role='grid']/descendant::tr[@data-uid and @aria-selected='true']"));
        if (choosenItem.size() > options.size()) {
            throw new WebActionException("Too many choosen item. Choosen ["
                    + choosenItem.size() + "] available [" + options.size()
                    + "]");
        }
        List<String> items = new ArrayList<>(
                (List<String>) (List<?>) choosenItem);
        Actions controlClick = new Actions(testManager.getWebDriver());

        // Clear selected item
        for (WebElement option : selectedOptions) {
            moveToElement(option);
            if (testManager.getBrowser().equals(Browsers.Safari)) {
                controlClickSafari(option);
            } else {
                controlClick.keyDown(Keys.CONTROL).click(option)
                        .keyUp(Keys.CONTROL).build().perform();
            }
        }

        for (WebElement option : options) {
            moveToElement(option);
            WebElement moduleElement = populateElement(option,
                    By.xpath("td[1]"));
            String value = moduleElement.getText();
            if (choosenItem.contains(value)) {
                if (testManager.getBrowser().equals(Browsers.Safari)) {
                    controlClickSafari(option);
                } else {
                    controlClick.keyDown(Keys.CONTROL).click(option)
                            .keyUp(Keys.CONTROL).build().perform();
                }
                items.remove(value);
            }
        }
        if (!items.isEmpty()) {
            throw new WebActionException("Some items not found");
        }
    }

    protected void controlClickSafari(WebElement element)
            throws WebActionException {
        testManager.getRemoteRobot().pressKey(KeyEvent.VK_META);
        Point positionOnScreen = ((RemoteWebElement) element).getCoordinates()
                .inViewPort();
        // REALLY DIRTY CODE I EVER WROTE
        int paddingLeft = testManager.getSafariPaddingLeft();
        int paddingTop = testManager.getSafariPaddingTop();
        testManager.getRemoteRobot().mouseClick(
                positionOnScreen.x + paddingLeft,
                positionOnScreen.y + paddingTop, InputEvent.BUTTON1_DOWN_MASK);
        testManager.getRemoteRobot().releaseKey(KeyEvent.VK_META);
    }

    /**
     * Handle multiselect list AsteRx project
     * 
     * @param element
     * @param choosenItem
     * @throws WebActionException
     */
    private void handleKendoChooseItemListAction(WebElement element,
            List<Object> choosenItem) throws WebActionException {
        List<WebElement> options = populateElements(
                element,
                By.xpath("preceding-sibling::*[@data-role='listview']/div[@data-uid]"));
        List<WebElement> selectedOptions = populateElements(
                element,
                By.xpath("preceding-sibling::*[@data-role='listview']/div[@data-uid and @aria-selected='true']"));
        if (choosenItem.size() > options.size()) {
            throw new WebActionException("Too many choosen item");
        }
        List<String> items = new ArrayList<>(
                (List<String>) (List<?>) choosenItem);
        Actions controlClick = new Actions(testManager.getWebDriver());

        // Clear selected item
        for (WebElement option : selectedOptions) {
            moveToElement(option);
            if (testManager.getBrowser().equals(Browsers.Safari)) {
                controlClickSafari(option);
            } else {
                controlClick.keyDown(Keys.CONTROL).click(option)
                        .keyUp(Keys.CONTROL).build().perform();
            }
        }

        for (WebElement option : options) {
            moveToElement(option);
            String value = option.getText();
            if (choosenItem.contains(value)) {
                if (testManager.getBrowser().equals(Browsers.Safari)) {
                    controlClickSafari(option);
                } else {
                    controlClick.keyDown(Keys.CONTROL).click(option)
                            .keyUp(Keys.CONTROL).build().perform();
                }
                items.remove(value);
            }
        }
        if (!items.isEmpty()) {
            throw new WebActionException("Some items not found");
        }
    }

    private void handleKendoCheckAction(WebElement element, String searchQuery)
            throws WebActionException {
        List<WebElement> rowElements = populateElements(
                element,
                By.xpath(XPATH_KENDO_ROW + "/"
                        + String.format(XPATH_SEARCH_BY_TEXT, searchQuery)));
        if (rowElements.isEmpty()) {
            LOG.error("Row not found");
            throw new WebDriverException("row detected = 0",
                    new NoSuchElementException("row detected = 0"));
        }
        for (WebElement row : rowElements) {
            WebElement checkButton = populateElement(
                    row,
                    By.xpath("preceding-sibling::td[@role='gridcell']/descendant::*[@class='mr-checkbox']"));
            handleClickAction(checkButton);
        }
    }

    /**
     * Specific delete appointment on MediRecord
     * 
     * @param element
     * @throws WebActionException
     */
    private void handleKendoDeleteAction(WebElement element)
            throws WebActionException {
        handleHoverAction(element);
        WebElement deleteButton = populateElement(element, By.xpath("span"));
        handleClickAction(deleteButton);
    }

    /**
     * Handle date time pick on KendoUI Framework
     * 
     * @param element
     * @param value
     * @throws ParseException
     * @throws WebActionException
     * @throws MalformedURLException
     */
    private void handleKendoDateTimeAction(WebElement element, String value)
            throws ParseException, WebActionException, MalformedURLException {
        // TODO identify value well..
        if (value.contains("/")) {
            handleKendoDateAction(element, value);
        }

        if (value.contains(":")) {
            WebElement hourButton = populateElement(element,
                    By.xpath("../descendant::*[contains(@class,'k-i-clock')]"));

            handleClickAction(hourButton);
            String query = "";
            if (value.contains("/")) {
                query = value.substring(value.indexOf(" ") + 1);
            } else {
                query = value;
            }

            // checking scroll needs
            WebElement hourOptions = populateElement(By.xpath("//ul[@id='"
                    + hourButton.getAttribute("aria-controls")
                    + "']/li[text()='" + query + "']"));
            try {
                populateElements(
                        null,
                        By.xpath("//ul[@id='"
                                + hourButton.getAttribute("aria-controls")
                                + "']/li"), true,
                        WebUIManager.getElementContentTimeout());
            } catch (TimeoutException e) {
                LOG.debug("Timeout exception - Scroll detected", e);
            }
            handleClickAction(hourOptions);
        }
    }

    /**
     * Handling kendo date click
     * 
     * @param element
     * @param value
     * @throws ParseException
     * @throws WebActionException
     */
    private void handleKendoDateAction(WebElement element, String value)
            throws ParseException, WebActionException {
        String optionFormat = "descendant::*[@data-value='%s/%s/%s']";

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat optionDateFormat = new SimpleDateFormat("yyyy/M/d");

        WebElement calendarButton = populateElement(element,
                By.xpath("../descendant::*[contains(@class,'k-i-calendar')]"));

        handleClickAction(calendarButton);

        String popupId = (calendarButton.getAttribute("aria-controls")) != null ? calendarButton
                .getAttribute("aria-controls") : element
                .getAttribute("aria-owns");

        WebElement datepickContainer = populateElement(By.xpath("//*[@id='"
                + popupId + "']"));

        // get navigation button

        WebElement prev = populateElement(datepickContainer,
                By.xpath("descendant::*[contains(@class,'k-nav-prev')]"));
        WebElement next = populateElement(datepickContainer,
                By.xpath("descendant::*[contains(@class,'k-nav-next')]"));
        WebElement fast = populateElement(datepickContainer,
                By.xpath("descendant::*[contains(@class,'k-nav-fast')]"));

        // normalize value
        int dateSectionNumber = value.split("/").length;
        String normalizeValue = value;
        boolean chooseDate = true;
        int changeMode = 2;
        // checking state of month
        boolean isMonthView = false;
        if (dateSectionNumber < 3) {
            normalizeValue = "01/" + normalizeValue;
            chooseDate = false;
            changeMode = 1;
            isMonthView = true;
        }

        Date date = dateFormat.parse(normalizeValue);
        Calendar todayCalendar = Calendar.getInstance();
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(date);

        int yearDiff = targetCalendar.get(Calendar.YEAR)
                - todayCalendar.get(Calendar.YEAR);
        // find differentiation of YEAR. if != 0 it's need to fast forwarded
        if (yearDiff != 0) {
            // change to decade view. 2 means, at the end of click, the view is
            // showing decades.

            for (int i = 0; i < changeMode; i++) {
                handleClickAction(fast);
            }

            // if differences more than 1 decade, we should move previous or
            // next decade
            int changeDecadeTimes = yearDiff / 10;
            if (changeDecadeTimes != 0) {
                WebElement movement = (changeDecadeTimes < 0) ? prev : next;
                for (int i = 0; i < Math.abs(changeDecadeTimes); i++) {
                    // special case for IE. Click should use jquery click.
                    handleClickAction(movement, testManager.getBrowser()
                            .equals(Browsers.IE));
                }
            }

            // now we should at right decade. Find the right options.
            Calendar yearTarget = Calendar.getInstance();
            yearTarget.setTime(targetCalendar.getTime());
            yearTarget.set(Calendar.MONTH, Calendar.JANUARY);
            yearTarget.set(Calendar.DAY_OF_MONTH, 1);
            String yearTargetString = String.format(optionFormat,
                    yearTarget.get(Calendar.YEAR), 0, 1);
            WebElement yearOptionTarget = populateElement(datepickContainer,
                    By.xpath(yearTargetString));
            handleClickAction(yearOptionTarget);
            // end of year selection. Current state calendar is showing month
            // options

            isMonthView = true;
        }

        int monthDiff = targetCalendar.get(Calendar.MONTH)
                - todayCalendar.get(Calendar.MONTH);
        if (monthDiff != 0) {
            if (!isMonthView) {
                // not in month view yet.
                handleClickAction(fast);
            }
            // now we should at right decade. Find the right options.
            Calendar monthTarget = Calendar.getInstance();
            monthTarget.setTime(targetCalendar.getTime());
            monthTarget.set(Calendar.DAY_OF_MONTH, 1);
            String monthTargetString = String.format(optionFormat,
                    monthTarget.get(Calendar.YEAR),
                    monthTarget.get(Calendar.MONTH), 1);
            WebElement monthOptionTarget = populateElement(datepickContainer,
                    By.xpath(monthTargetString));
            handleClickAction(monthOptionTarget);
            // finish choosing month, now view is date on right month.
        }
        if (chooseDate) {
            String dayTargetString = String.format(optionFormat,
                    targetCalendar.get(Calendar.YEAR),
                    targetCalendar.get(Calendar.MONTH),
                    targetCalendar.get(Calendar.DATE));
            WebElement dayOptionTarget = populateElement(datepickContainer,
                    By.xpath(dayTargetString));
            handleClickAction(dayOptionTarget);
        }
    }

    /**
     * Handle kendo expand row at grid
     * 
     * @param element
     * @param rowIdentifier
     * @throws WebActionException
     */
    private void handleKendoExpandRowAction(WebElement element,
            String rowIdentifier) throws WebActionException {
        moveToElement(element);
        List<WebElement> rowElements = populateElements(
                element,
                By.xpath(XPATH_KENDO_ROW
                        + String.format(XPATH_SEARCH_BY_TEXT, rowIdentifier)));
        if (rowElements.isEmpty()) {
            throw new NoSuchElementException("row detected = 0");
        }

        for (WebElement row : rowElements) {
            WebElement expandButton = populateElement(
                    row,
                    By.xpath("preceding-sibling::td[@class='k-hierarchy-cell']/a"));
            handleClickAction(expandButton);
        }
    }

    /**
     * Handle kendo custom dropdown.
     * 
     * @param element
     * @param selectedValue
     * @throws WebActionException
     * @throws MalformedURLException
     */
    private void handleKendoDropdownAction(WebElement element,
            String selectedValue) throws WebActionException,
            MalformedURLException {
        WebElement dropdownField = populateElement(By
                .xpath("//descendant::input[@id='" + element.getAttribute("id")
                        + "']/.."));
        handleClickAction(dropdownField);
        WebElement optionContainer = populateElement(By.xpath("//ul[@id='"
                + dropdownField.getAttribute("aria-owns")
                + "']/li[normalize-space(text())='" + selectedValue + "']"));
        try {
            populateElements(null, By.xpath("//ul[@id='"
                    + dropdownField.getAttribute("aria-owns") + "']/li"), true,
                    WebUIManager.getElementContentTimeout());
        } catch (TimeoutException e) {
            LOG.debug("Timeout exception - Scroll detected", e);
            // moveToElement(optionContainer);
        }
        handleClickAction(optionContainer);
    }

    /**
     * special enter case for asterx numeric field
     * 
     * @param element
     * @param content
     * @throws WebActionException
     */
    private void handleAsterxEnterNumeric(WebElement element, int content)
            throws WebActionException {
        WebElement visibleField = populateElement(element,
                By.xpath("preceding-sibling::input[1]"));
        handleClickAction(visibleField);
        handleEnterAction(element, String.valueOf(content));
    }

    /**
     * special click case for asterx check box
     * 
     * @param element
     * @throws WebActionException
     */
    private void handleAsterxCheckbox(WebElement element)
            throws WebActionException {
        WebElement clickableElement = populateElement(element,
                By.xpath("ancestor::*[contains(@class,'mr-checkbox')]"));
        handleClickAction(clickableElement);
    }

    /**
     * special click case for asterx radio button
     * 
     * @param element
     * @throws WebActionException
     */
    private void handleAsterxRadioButton(WebElement element)
            throws WebActionException {
        WebElement clickableElement = populateElement(element,
                By.xpath("ancestor::*[contains(@class,'mr-radiobutton')]"));
        handleClickAction(clickableElement);
    }

    protected boolean handleAppointmentLocationValidation(
            DomElement domElement, DomElementExpectation expectation) {
        try {
            WebElement appointmentElement = populateElement(domElement);
            List<String> parameters = (List<String>) expectation.getValue();
            WebElement grid = populateElement(appointmentElement,
                    By.xpath("ancestor::*[@data-role='scheduler']"));
            WebElement targetCell = getSchedulerCell(grid, parameters.get(0),
                    parameters.get(1));
            return (boolean) testManager.getJQueryDriver().executeScript(
                    testManager.getResource("insideElement.js"), targetCell,
                    appointmentElement);
        } catch (WebActionException e) {
            LOG.error("Validation error", e);
            return false;
        }
    }

    /**
     * Performing double click on {@link SchedulerCell} model for Appointment
     * Grid
     * 
     * @param schedule
     * @param grid
     * @throws WebActionException
     */
    protected void handleDoubleClickCellAction(WebElement grid, String time,
            String doctor) throws WebActionException {
        handleDoubleClickAction(getSchedulerCell(grid, time, doctor));
    }

    /**
     * Get row number of Appointment Grid
     * 
     * @param time
     * @param grid
     * @return
     */
    private int getRowByTime(String time, WebElement grid) {
        List<WebElement> hourLabels = populateElements(grid,
                By.xpath(XPATH_APPOINTMENT_SCHEDULER_TIME));
        int result = -1;
        for (int i = 0; i < hourLabels.size() && result == -1; i++) {
            moveToElement(hourLabels.get(i));
            if (hourLabels.get(i).getText().contains(time)) {
                result = i;
            }
        }
        return result;
    }

    /**
     * Get Column number of Appointment Grid
     * 
     * @param doctor
     * @param grid
     * @return
     */
    private int getColByDoctor(String doctor, WebElement grid) {
        List<WebElement> doctorLabels = populateElements(grid,
                By.xpath(XPATH_APPOINTMENT_DOCTORS));
        int result = -1;
        for (int i = 0; i < doctorLabels.size() && result == -1; i++) {
            moveToElement(doctorLabels.get(i));
            if (doctorLabels.get(i).getText().contains(doctor)) {
                result = i;
            }
        }
        return result;
    }

    private WebElement getSchedulerCell(WebElement grid, String time,
            String doctor) throws WebActionException {
        try {
            int row = getRowByTime(time, grid) + 1;
            int col = getColByDoctor(doctor, grid) + 1;
            if (row > 0 && col > 0) {
                String queryXpath = String.format(XPATH_APPOINTMENT_CELL, row,
                        col);
                return populateElement(grid, By.xpath(queryXpath));
            } else {
                throw new WebActionException("Invalid cell element");
            }
        } catch (WebActionException e) {
            throw new WebActionException("Failed to doubleclick at " + grid, e);
        }
    }

    @Override
    protected boolean valueCheck(HtmlActionCondition htmlAction,
            DomElementExpectation expectation, DomElement domElement) {
        String htmlValue = getDomElementHtmlValue(domElement);

        htmlValue = htmlValue.replace(Constants.NBSP, Constants.WHITESPACE)
                .trim();

        boolean result = false;
        if (htmlAction instanceof GenericHtmlActionConditionEnum) {
            result = super.valueCheck(htmlAction, expectation, domElement);
        } else {
            switch ((AsterxHtmlActionConditionEnum) htmlAction) {
            case MEMBER_DROPDOWN_CONTENT:
                result = handleMemberDropdownContentAction(domElement,
                        expectation);
                break;
            case MEMBER_DROPDOWN_CONTAIN:
                result = handleMemberDropdownContainAction(domElement,
                        expectation);
                break;
            case MEMBER_MULTISELECT_GRID_CONTENT:
                result = handleMemberGridContentAction(domElement, expectation);
                break;
            case MEMBER_MULTISELECT_GRID_CONTAIN:
                result = handleMemberGridContainAction(domElement, expectation);
                break;
            case MEMBER_MULTISELECT_LIST_CONTENT:
                result = handleMemberListContentAction(domElement, expectation);
                break;
            case MEMBER_MULTISELECT_LIST_CONTAIN:
                result = handleMemberListContainAction(domElement, expectation);
                break;
            case MEMBER_MULTISELECT_SELECTED_CONTENT:
                result = handleMultiselectSelectedContentAction(domElement,
                        expectation);
                break;
            case MEMBER_MULTISELECT_OPTION_CONTENT:
                result = handleMultiselectOptionContentAction(domElement,
                        expectation);
                break;
            case MEMBER_MULTISELECT_OPTION_CONTAINS:
                result = handleMultiselectOptionContainsAction(domElement,
                        expectation);
                break;
            case MEMBER_MULTISELECT_SELECTED_CONTAINS:
                result = handleMultiselectSelectedContainsAction(domElement,
                        expectation);
                break;
            case APPOINTMENT_LOCATION_VALIDATION:
                result = handleAppointmentLocationValidation(domElement,
                        expectation);
                break;
            default:
                result = super.valueCheck(htmlAction, expectation, domElement);
                break;
            }
        }

        return result;
    }

    protected boolean handleMemberDropdownContentAction(DomElement domElement,
            DomElementExpectation elementExpectation) {
        List<String> dropdownMemberExpectation = (List<String>) elementExpectation
                .getValue();
        WebElement dropdownInput = populateElement(domElement);
        WebElement dropdownField = populateElement(By
                .xpath("//descendant::input[@id='"
                        + dropdownInput.getAttribute("id") + "']/.."));
        try {
            handleClickAction(dropdownField);
        } catch (WebActionException e) {
            LOG.error("Not valid dropdown");
            return false;
        }
        List<WebElement> dropdownMemberActual = populateElements(By
                .xpath("//ul[@id='" + dropdownField.getAttribute("aria-owns")
                        + "']/li"));
        // checking size of member
        if (dropdownMemberExpectation.size() == dropdownMemberActual.size()) {
            for (int i = 0; i < dropdownMemberActual.size(); i++) {
                WebElement actualMember = dropdownMemberActual.get(i);
                String expectMember = dropdownMemberExpectation.get(i);
                moveToElement(actualMember);
                if (!actualMember.getText().equals(expectMember)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    protected boolean handleMemberDropdownContainAction(DomElement domElement,
            DomElementExpectation elementExpectation) {
        List<String> dropdownMemberExpectation = (List<String>) elementExpectation
                .getValue();
        List<String> dropdownCheck = new ArrayList<>(dropdownMemberExpectation);
        WebElement dropdownInput = populateElement(domElement);
        WebElement dropdownField = populateElement(By
                .xpath("//descendant::input[@id='"
                        + dropdownInput.getAttribute("id") + "']/.."));
        try {
            handleClickAction(dropdownField);
        } catch (WebActionException e) {
            LOG.error("Not valid dropdown");
            return false;
        }
        List<WebElement> dropdownMemberActual = populateElements(By
                .xpath("//ul[@id='" + dropdownField.getAttribute("aria-owns")
                        + "']/li"));
        // checking size of member
        for (int i = 0; i < dropdownMemberActual.size(); i++) {
            WebElement actualMember = dropdownMemberActual.get(i);
            moveToElement(actualMember);
            // removing correspond value
            handleWaitAction(WebUIManager.getElementPoolingTimeout());
            dropdownCheck.remove(actualMember.getText());
        }
        if (dropdownCheck.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    protected boolean handleMemberGridContentAction(DomElement domElement,
            DomElementExpectation elementExpectation) {
        WebElement element = populateElement(domElement);
        List<WebElement> options = populateElements(
                element,
                By.xpath("preceding-sibling::*[@data-role='grid']/descendant::tr[@data-uid]"));
        List<String> choosenItem = (List<String>) elementExpectation.getValue();
        List<String> items = new ArrayList<>(choosenItem);

        if (options.size() != choosenItem.size()) {
            LOG.error("Option size not valid");
            return false;
        }

        for (int i = 0; i < options.size(); i++) {
            WebElement option = options.get(i);
            String current = choosenItem.get(i);
            moveToElement(option);
            WebElement moduleElement = populateElement(option,
                    By.xpath(FIRST_TD));
            String value = moduleElement.getText();
            if (current.equals(value)) {
                items.remove(value);
            } else {
                LOG.error("Checking content error, expected: [" + current
                        + "] actual: [" + value + "];");
                return false;
            }
        }
        return true;
    }

    protected boolean handleMemberGridContainAction(DomElement domElement,
            DomElementExpectation elementExpectation) {
        WebElement element = populateElement(domElement);
        List<WebElement> options = populateElements(
                element,
                By.xpath("preceding-sibling::*[@data-role='grid']/descendant::tr[@data-uid]"));
        List<String> choosenItem = (List<String>) elementExpectation.getValue();
        List<String> items = new ArrayList<>(choosenItem);

        for (WebElement option : options) {
            moveToElement(option);
            WebElement moduleElement = populateElement(option,
                    By.xpath(FIRST_TD));
            String value = moduleElement.getText();
            if (choosenItem.contains(value)) {
                items.remove(value);
            }
        }
        if (!items.isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * Handle expectation for checking if the multi select list contains given
     * objects
     * 
     * @param domElement
     * @param elementExpectation
     * @return
     */
    protected boolean handleMemberListContainAction(DomElement domElement,
            DomElementExpectation elementExpectation) {
        // get web element from params
        WebElement element = populateElement(domElement);

        // populate all options
        List<WebElement> options = populateElements(
                element,
                By.xpath("preceding-sibling::*[@data-role='listview']/div[@data-uid]"));

        // get expectation list from params
        List<String> expectationList = (List<String>) elementExpectation
                .getValue();
        // create copy of expectation list to validate condition
        List<String> items = new ArrayList<>(expectationList);

        for (WebElement option : options) {
            moveToElement(option);
            String value = option.getText();
            if (expectationList.contains(value)) {
                items.remove(value);
            }
        }

        // if expectation list still have remaining items, fail
        if (!items.isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * Handle expectation for checking if the multiselect list has equal content
     * with given params
     * 
     * @param domElement
     * @param elementExpectation
     * @return
     */
    protected boolean handleMemberListContentAction(DomElement domElement,
            DomElementExpectation elementExpectation) {
        // get web element from params
        WebElement element = populateElement(domElement);

        // populate all options
        List<WebElement> options = populateElements(
                element,
                By.xpath("preceding-sibling::*[@data-role='listview']/div[@data-uid]"));

        // get expectation list from params
        List<String> expectationList = (List<String>) elementExpectation
                .getValue();

        if (options.size() != expectationList.size()) {
            // if the member of expectation list doesn't same with available
            // options, fail
            LOG.error("Different total of member");
            return false;
        }

        for (int i = 0; i < options.size(); i++) {
            WebElement option = options.get(i);
            moveToElement(option);
            String current = expectationList.get(i);
            String value = option.getText();
            if (!current.equals(value)) {
                return false;
            }
        }
        return true;
    }

    protected boolean handleMultiselectOptionContentAction(
            DomElement domElement, DomElementExpectation expectation) {
        WebElement element = populateElement(domElement);
        WebElement inputMultiSelect = populateElement(
                element,
                By.xpath("preceding-sibling::*[contains(@class,'k-multiselect-wrap')]/input"));
        String[] owns = inputMultiSelect.getAttribute("aria-owns").split(
                Constants.WHITESPACE);
        String idAvailableOption = owns[1];
        WebElement handlerMultiSelect = populateElement(
                element,
                By.xpath("preceding-sibling::*[contains(@class,'k-multiselect-wrap')]"));
        // Create copy of expected value
        List<String> expectedList = (List<String>) expectation.getValue();
        try {
            handleClickAction(handlerMultiSelect);
            List<WebElement> targetOptions = populateElements(By
                    .xpath("//ul[@id='" + idAvailableOption
                            + "']/li[not(contains(@style,'display: none'))]"));

            if (targetOptions.size() != expectedList.size()) {
                LOG.error("Available options number doesn't same with expected");
                return false;
            }

            for (int i = 0; i < targetOptions.size(); i++) {
                moveToElement(targetOptions.get(i));
                String actualValue = targetOptions.get(i).getText();
                String expectedValue = expectedList.get(i);
                if (!actualValue.equals(expectedValue)) {
                    return false;
                }
                Thread.sleep(WebUIManager.getElementPoolingTimeout());
            }
        } catch (WebActionException e) {
            LOG.error("Failed to open multiselect option");
            return false;
        } catch (InterruptedException e) {
            LOG.error("Interrupted wait");
            return false;
        }
        return true;
    }

    protected boolean handleMultiselectOptionContainsAction(
            DomElement domElement, DomElementExpectation expectation) {
        WebElement element = populateElement(domElement);
        WebElement inputMultiSelect = populateElement(
                element,
                By.xpath("preceding-sibling::*[contains(@class,'k-multiselect-wrap')]/input"));
        String[] owns = inputMultiSelect.getAttribute("aria-owns").split(
                Constants.WHITESPACE);
        String idAvailableOption = owns[1];
        WebElement handlerMultiSelect = populateElement(
                element,
                By.xpath("preceding-sibling::*[contains(@class,'k-multiselect-wrap')]"));
        // Create copy of expected value
        List<String> expectedList = (List<String>) expectation.getValue();
        List<String> items = new ArrayList<>(expectedList);
        try {
            handleClickAction(handlerMultiSelect);
            List<WebElement> targetOptions = populateElements(By
                    .xpath("//ul[@id='" + idAvailableOption + "']/li"));
            for (WebElement option : targetOptions) {
                moveToElement(option);
                String actualValue = option.getText();
                if (expectedList.contains(actualValue)) {
                    items.remove(actualValue);
                }
                Thread.sleep(WebUIManager.getElementPoolingTimeout());
            }
        } catch (WebActionException e) {
            LOG.error("Failed to open multiselect option", e);
            return false;
        } catch (InterruptedException e) {
            LOG.error("Interrupted wait", e);
            return false;
        }
        return true;
    }

    protected boolean handleMultiselectSelectedContentAction(
            DomElement domElement, DomElementExpectation expectation) {
        WebElement element = populateElement(domElement);
        WebElement inputMultiSelect = populateElement(
                element,
                By.xpath("preceding-sibling::*[contains(@class,'k-multiselect-wrap')]/input"));
        String[] owns = inputMultiSelect.getAttribute("aria-owns").split(
                Constants.WHITESPACE);
        String idSelectedOption = owns[0];

        // Create copy of expected value
        List<String> expectedList = (List<String>) expectation.getValue();
        List<WebElement> options = populateElements(By.xpath("//ul[@id='"
                + idSelectedOption + "']/li"));

        if (options.size() != expectedList.size()) {
            return false;
        }

        for (int i = 0; i < options.size(); i++) {
            WebElement content = populateElement(options.get(i),
                    By.xpath("descendant::span[1]"));
            String contentString = content.getText();
            String expectedString = expectedList.get(i);
            if (!contentString.equals(expectedString)) {
                return false;
            }
        }
        return true;
    }

    protected boolean handleMultiselectSelectedContainsAction(
            DomElement domElement, DomElementExpectation expectation) {
        WebElement element = populateElement(domElement);
        WebElement inputMultiSelect = populateElement(
                element,
                By.xpath("preceding-sibling::*[contains(@class,'k-multiselect-wrap')]/input"));
        String[] owns = inputMultiSelect.getAttribute("aria-owns").split(
                Constants.WHITESPACE);
        String idSelectedOption = owns[0];

        // Create copy of expected value
        List<String> expectedList = (List<String>) expectation.getValue();
        List<String> items = new ArrayList<>(expectedList);
        List<WebElement> options = populateElements(By.xpath("//ul[@id='"
                + idSelectedOption + "']/li"));

        for (WebElement option : options) {
            WebElement content = populateElement(option,
                    By.xpath("descendant::span[1]"));
            String contentString = content.getText();
            if (expectedList.contains(contentString)) {
                items.remove(contentString);
            }
        }

        if (!items.isEmpty()) {
            return false;
        }

        return true;
    }

    /**
     * Moving screen to correspond Element.
     * 
     * @param element
     */
    @Override
    protected void moveToElement(WebElement element) {
        LOG.debug("start " + element + " visibility : " + isVisible(element));
        if (!isVisible(element)) {
            boolean customContainer = false;
            try {
                WebElement container = populateElement(
                        element,
                        By.xpath("ancestor::*[contains(@class,'mCustomScrollBox')]/descendant::*[@class='mCSB_draggerRail']"));
                customContainer = container.isDisplayed();
            } catch (TimeoutException e) {
            }
            moveTo(element, customContainer);
            if (!isVisible(element) && customContainer) {
                customScroll(element);
            }
        }
        LOG.debug("End " + element + " visibility : " + isVisible(element));
    }

    @Override
    public HtmlActionStatus doAction(ActionRequest actionRequest)
            throws WebActionException {

        HtmlAction action = actionRequest.getAction();
        if (action instanceof GenericHtmlAction) {
            action = (GenericHtmlAction) action;
        } else {
            action = (AsteRxHtmlAction) action;
        }
        // Applying action pace delay
        handleWaitAction(WebUIManager.getActionPace());
        jqueryActiveChecking();

        // Non element action
        HtmlActionStatus nonElementActionStatus = handleNonElementAction(
                actionRequest, action);
        if (!nonElementActionStatus.equals(HtmlActionStatus.NOT_SUPPORTED)) {
            return nonElementActionStatus;
        }
        // Element action
        WebElement element = populateElement(actionRequest
                .getElementIdentifier());

        // get condition
        boolean fail = false;
        try {
            HtmlActionStatus result = handleAction(element, action,
                    actionRequest.getParameters());
            if (result == HtmlActionStatus.NOT_SUPPORTED) {
                result = customAction(actionRequest);
            }
            return result;
        } catch (WebActionException e) {
            LOG.error("Exception on executing action", e);
            fail = true;
            // only take screenshot for action specific to asterx, other handled
            // by generic
            doScreenshot(((AsteRxHtmlAction) action).name(), fail);
            throw new WebActionException("Exception on executing action", e);

        }
    }

}
