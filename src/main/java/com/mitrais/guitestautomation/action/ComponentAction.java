package com.mitrais.guitestautomation.action;

import com.mitrais.guitestautomation.page.DomElementExpectation;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.List;

public class ComponentAction {
    private String componentName;
    private GenericHtmlAction action;
    private String actionValue = null; // only apply to ENTER/SELECT as action
    @JsonIgnore
    private boolean optional;
    @JsonIgnore
    private List<DomElementExpectation> expects;

    public void setAction(String actionName) {
        if (actionName.equalsIgnoreCase(GenericHtmlAction.ENTER.getActionName())) {
            action = GenericHtmlAction.ENTER;
        } else if (actionName.equalsIgnoreCase(GenericHtmlAction.SELECT
                .getActionName())) {
            action = GenericHtmlAction.SELECT;
        } else if (actionName
                .equalsIgnoreCase(GenericHtmlAction.CLICK.getActionName())) {
            action = GenericHtmlAction.CLICK;
        } else if (actionName.equalsIgnoreCase(GenericHtmlAction.REFRESH
                .getActionName())) {
            action = GenericHtmlAction.REFRESH;
        } else if (actionName
                .equalsIgnoreCase(GenericHtmlAction.HOVER.getActionName())) {
            action = GenericHtmlAction.HOVER;
        } else if (actionName.equalsIgnoreCase(GenericHtmlAction.WAIT.getActionName())) {
            action = GenericHtmlAction.WAIT;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ComponentAction)) {
            return false;
        } else {
            return ((ComponentAction) o).getComponentName().equals(
                    componentName)
                    && ((ComponentAction) o).getAction().getActionName()
                            .equals(action.getActionName());
        }
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public String serialize() {
        return componentName + '.' + action.getActionName().toUpperCase();
    }

    public String toString() {
        StringBuilder returnStringBuffer = new StringBuilder();

        returnStringBuffer.append(componentName).append('.');

        if (action == GenericHtmlAction.ENTER) {
            returnStringBuffer
                    .append(GenericHtmlAction.ENTER.getActionName().toUpperCase())
                    .append('(').append(actionValue).append(')');
        } else if (action == GenericHtmlAction.SELECT) {
            returnStringBuffer
                    .append(GenericHtmlAction.SELECT.getActionName().toUpperCase())
                    .append('(').append(actionValue).append(')');
        } else {
            returnStringBuffer.append(action.getActionName().toUpperCase());
        }

        returnStringBuffer.append(DomElementExpectation
                .normalizeExpects(expects));

        return returnStringBuffer.toString();
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    public void setActionValue(String actionValue) {
        this.actionValue = actionValue;
    }

    public String getComponentName() {
        return componentName;
    }

    public GenericHtmlAction getAction() {
        return action;
    }

    public String getActionValue() {
        return actionValue;
    }

    public boolean isOptional() {
        return optional;
    }
}
