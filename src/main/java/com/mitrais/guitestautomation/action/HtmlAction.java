package com.mitrais.guitestautomation.action;


public interface HtmlAction {
	public String getActionName();
	public String toString();

}
