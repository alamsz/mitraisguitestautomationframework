package com.mitrais.guitestautomation.action;

public enum HtmlActionStatus {
    CONDITION_NOT_MET("condition not met"), 
    SHOW_STOPPER("showstopper"), 
    SKIP_THIS_PAGE("skip"), 
    GOOD("good"),
    NOT_SUPPORTED("not supported");

    private String value;

    HtmlActionStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
