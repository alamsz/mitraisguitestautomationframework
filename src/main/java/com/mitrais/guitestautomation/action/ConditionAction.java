package com.mitrais.guitestautomation.action;

public class ConditionAction {
    private String componentName;
    private GenericHtmlAction action;

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public GenericHtmlAction getAction() {
        return action;
    }

    public void setAction(GenericHtmlAction action) {
        this.action = action;
    }
}
