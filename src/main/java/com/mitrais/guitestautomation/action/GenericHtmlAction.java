package com.mitrais.guitestautomation.action;

public enum GenericHtmlAction implements HtmlAction {
	CLICK("click"), 
	HOVER("hover"), 
	WAIT("wait"), 
	REFRESH("refresh"), 
	ENTER("enter"),
	SELECT("select"), 
	DOUBLE_CLICK("double-click"), 
	DRAG("drag"), 
	UPLOAD("upload"),
	CHOOSE_FILE("choose_file");

	private String actionName;

	GenericHtmlAction(String actionName) {
		this.actionName = actionName;
	}

	public String getActionName() {
		return actionName;
	}

	public String toString() {
		return getActionName().toUpperCase();
	}
}
