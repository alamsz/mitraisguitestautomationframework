package com.mitrais.guitestautomation.keyworddefinition;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.mitrais.guitestautomation.action.GenericHtmlAction;
import com.mitrais.guitestautomation.action.HtmlAction;
import com.mitrais.guitestautomation.action.HtmlActionStatus;
import com.mitrais.guitestautomation.exception.IllegalCucumberFormatException;
import com.mitrais.guitestautomation.exception.WebActionException;
import com.mitrais.guitestautomation.page.DomElementExpectation;
import com.mitrais.guitestautomation.page.GenericHtmlActionConditionEnum;
import com.mitrais.guitestautomation.processor.ActionProcessor;
import com.mitrais.guitestautomation.processor.ActionRequest;
import com.mitrais.guitestautomation.processor.GenericActionProcessor;
import com.mitrais.guitestautomation.util.Utils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MUITStepDefinition {

    protected static ActionProcessor processor = GenericActionProcessor.getInstance();
    protected Scenario scenario;

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
        processor.setScenario(scenario);
    }

    

    @After("@last")
    public void cleanUp() throws InterruptedException, WebActionException {
        processor.terminateProcessor();
    }

    
    @Given("^I am ON \"(.*?)\"$")
    public void iamOn(String arg1) throws WebActionException {
        processor.setup(arg1);
        assertTrue("no exception on setup", true);
    }

    @Then("^I should EXPECT$")
    public void shouldExpect(DataTable data) throws WebActionException,
            IllegalCucumberFormatException {
        for (DomElementExpectation elementExpectation : Utils.expectBridge(data)) {
            assertTrue(processor.doExpect(elementExpectation));
        }
    }

   

    @Given("^I have hovered \"(.*?)\"$")
    @When("^I hover \"(.*?)\"$")
    public void i_hover(String identifier) throws Throwable {
        ActionRequest request = new ActionRequest(GenericHtmlAction.HOVER, identifier,
                scenario.getName() + " Hover");
        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
    }

    @Given("^I have clicked \"(.*?)\"$")
    @When("^I click \"(.*?)\"$")
    public void click(String identifier) throws WebActionException {
        ActionRequest request = new ActionRequest(GenericHtmlAction.CLICK,
                Utils.normalizeIdentifier(identifier), scenario.getName()
                        + " Click");
        HtmlActionStatus status = processor.doAction(request);
        assertEquals(HtmlActionStatus.GOOD, status);
    }

   

    @Given("^I have entered$")
    @When("^I enter$")
    public void enter(DataTable data) throws WebActionException {
        Map<String, String> dataEntry = data.asMap(String.class, String.class);
        enterAction(dataEntry);
    }

    
    protected void enterAction(Map<String, String> dataEntry)
            throws WebActionException {
        for (String identifier : dataEntry.keySet()) {
            ActionRequest request = new ActionRequest(GenericHtmlAction.ENTER,
            		Utils.normalizeIdentifier(identifier),
                    scenario.getName() + " Enter");
            request.addParameters(dataEntry.get(identifier));
            HtmlActionStatus status = processor.doAction(request);
            assertEquals(HtmlActionStatus.GOOD, status);
        }
    }

   
    @Then("^I should be ON \"(.*?)\"$")
    public void shouldBeOn(String arg1) throws WebActionException,
            MalformedURLException {
        processor.setup(arg1);
        assertTrue("no exception on setup", true);
    }

    @Given("^I have selected$")
    @When("^I select$")
    public void select(DataTable data) throws WebActionException {
        Map<String, String> dataEntry = data.asMap(String.class, String.class);
        for (String identifier : dataEntry.keySet()) {
            ActionRequest request = new ActionRequest(GenericHtmlAction.SELECT,
            		Utils.normalizeIdentifier(identifier),
                    scenario.getName() + " Select");
            request.addParameters(dataEntry.get(identifier));
            assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
        }
    }

    @Given("^I have waited (\\d+) seconds$")
    @When("^I wait (\\d+) seconds$")
    public void waitSeconds(int arg1) throws WebActionException {
        ActionRequest request = new ActionRequest(GenericHtmlAction.WAIT, null,
                scenario.getName() + " wait");
        request.addParameters(arg1);
        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
    }

   

   

    @Given("^I have refreshed page$")
    @When("^I refresh page$")
    public void refreshPage() throws WebActionException {
        ActionRequest request = new ActionRequest(GenericHtmlAction.REFRESH, null,
                scenario.getName() + " Refresh page");
        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
    }

    
    @Given("^I have double-clicked ON \"(.*?)\"$")
    @When("^I double-click ON \"(.*?)\"$")
    public void doubleClickOn(String identifier) throws WebActionException {
        ActionRequest request = new ActionRequest(GenericHtmlAction.DOUBLE_CLICK,
        		Utils.normalizeIdentifier(identifier), scenario.getName()
                        + " Double click on");
        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
    }

   
    @Given("^I have uploaded$")
    @When("^I upload$")
    public void upload(DataTable arg1) throws WebActionException {
        Map<String, String> dataTable = arg1.asMap(String.class, String.class);
        for (String key : dataTable.keySet()) {
            ActionRequest request = new ActionRequest(GenericHtmlAction.UPLOAD,
            		Utils.normalizeIdentifier(key), scenario.getName()
                            + " upload");
            request.addParameters(dataTable.get(key));
            assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
        }
    }

   
    @Given("^I have choose file \"(.*?)\" in upload \"(.*?)\" and \"(.*?)\"$")
    @When("^I choose file \"(.*?)\" in upload \"(.*?)\" and \"(.*?)\"$")
    public void chooseFile(String filePath, String fieldId, String buttonId)
            throws WebActionException {
        ActionRequest request = new ActionRequest(GenericHtmlAction.CHOOSE_FILE, null,
                scenario.getName() + " check");
        request.addParameters(filePath);
        request.addParameters(fieldId);
        request.addParameters(buttonId);
        assertEquals(HtmlActionStatus.GOOD, processor.doAction(request));
    }
   
   
}
