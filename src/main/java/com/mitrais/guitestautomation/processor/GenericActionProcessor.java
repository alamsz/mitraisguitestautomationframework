package com.mitrais.guitestautomation.processor;

import static com.mitrais.guitestautomation.util.Constants.$_WINDOW_HEIGHT;
import static com.mitrais.guitestautomation.util.Constants.ARGUMENTS_0;
import static com.mitrais.guitestautomation.util.Constants.ARGUMENTS_1;
import static com.mitrais.guitestautomation.util.Constants.COMPLETE;
import static com.mitrais.guitestautomation.util.Constants.DOCUMENT_READY_STATE;
import static com.mitrais.guitestautomation.util.Constants.EMPTY_STRING;
import static com.mitrais.guitestautomation.util.Constants.INNER_HTML;
import static com.mitrais.guitestautomation.util.Constants.J_QUERY_ACTIVE;
import static com.mitrais.guitestautomation.util.Constants.NBSP;
import static com.mitrais.guitestautomation.util.Constants.SMALL_AMOUNT;
import static com.mitrais.guitestautomation.util.Constants.WHITESPACE;
import static com.mitrais.guitestautomation.util.Constants._0_9_REGEX;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.MoveTargetOutOfBoundsException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.mitrais.guitestautomation.action.GenericHtmlAction;
import com.mitrais.guitestautomation.action.HtmlAction;
import com.mitrais.guitestautomation.action.HtmlActionStatus;
import com.mitrais.guitestautomation.controller.WebUIManager;
import com.mitrais.guitestautomation.exception.WebActionException;
import com.mitrais.guitestautomation.page.DomElement;
import com.mitrais.guitestautomation.page.DomElementExpectation;
import com.mitrais.guitestautomation.page.GenericHtmlActionConditionEnum;
import com.mitrais.guitestautomation.page.HtmlActionCondition;
import com.mitrais.guitestautomation.page.Page;
import com.mitrais.guitestautomation.util.Browsers;
import com.mitrais.guitestautomation.util.Constants.StringRef;
import com.mitrais.guitestautomation.util.Utils;

import cucumber.api.Scenario;

/**
 * Class that represent how to do Action in {@link Page}
 * 
 * @author Aldira_R
 *
 */
public class GenericActionProcessor implements ActionProcessor {

    private static final String TYPEOF_J_QUERY_UNDEFINED = "typeof jQuery !== 'undefined'";
    public static final String ARGUMENTS_0_IS_VISIBLE_ARGUMENTS_0 = ARGUMENTS_0
            + ".isVisible(" + ARGUMENTS_0 + ")";

    private static final Logger LOG = LoggerFactory
            .getLogger(GenericActionProcessor.class);
    protected static By self = By.xpath(".");
    protected Scenario scenario;

    protected GenericActionProcessor() {
        testManager = WebUIManager.getInstance();
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }

    public Scenario getScenario() {
        return scenario;
    }

    /**
     * Page to be tested
     */
    protected Page testedPage;

    /**
     * Test Manager that contains timeout, WebDriver, etc.
     */
    protected WebUIManager testManager;

    /**
     * Setup the page and validating page
     * 
     * @param pageIdentifier
     * @throws WebActionException
     */
    public void setup(String pageIdentifier) throws WebActionException {
        testedPage = testManager.getPage(pageIdentifier);
        boolean isfail = false;
        try {
            new WebDriverWait(testManager.getWebDriver(),
                    TimeUnit.SECONDS.convert(WebUIManager.getPageLoadTimeout(),
                            TimeUnit.MILLISECONDS))
                    .until(new ExpectedCondition<Boolean>() {

                        long lastJqueryActive = 0;

                        @Override
                        public Boolean apply(WebDriver input) {
                            // checking state by js
                            if (!((JavascriptExecutor) input).executeScript(
                                    MessageFormat.format(
                                            StringRef.RETURN_FORMAT,
                                            DOCUMENT_READY_STATE)).equals(
                                    COMPLETE)) {
                                return false;
                            }

                            // checking title of page
                            boolean doTitle = (testedPage.getTitle() != null)
                                    && !testedPage.getTitle().trim().isEmpty();

                            boolean urlCorrect = input.getCurrentUrl()
                                    .contains(testedPage.getUrlExtension());
                            boolean urlAndTitleCorrect = doTitle ? urlCorrect
                                    && input.getTitle().equals(
                                            testedPage.getTitle()) : urlCorrect;
                            boolean keyComponentExisted = true;

                            if (testedPage.getKeyDomElementName() != null) {
                                WebElement keyDomComponent = populateElement(testedPage
                                        .getKeyDomElementName());

                                if (keyDomComponent == null) {
                                    LOG.info("There is no such domElement, ["
                                            + testedPage.getKeyDomElementName()
                                            + "] in your configuration.");

                                    return false;
                                }

                                if (!keyDomComponent.isEnabled()
                                        || !keyDomComponent.isDisplayed()) {
                                    keyComponentExisted = false;
                                }
                            }
                            if (urlAndTitleCorrect && keyComponentExisted) {
                                boolean isJqueryAvailable = (boolean) testManager
                                        .getJQueryDriver()
                                        .executeScript(
                                                MessageFormat
                                                        .format(StringRef.RETURN_FORMAT,
                                                                TYPEOF_J_QUERY_UNDEFINED));
                                if (!isJqueryAvailable) {
                                    WebUIManager.loadJQuery(testManager
                                            .getJQueryDriver());
                                }
                                loadJSLibrary();
                            }

                            long currentJqueryActive = (long) testManager
                                    .getJQueryDriver().executeScript(
                                            MessageFormat.format(
                                                    StringRef.RETURN_FORMAT,
                                                    J_QUERY_ACTIVE));
                            if (lastJqueryActive != currentJqueryActive) {
                                lastJqueryActive = currentJqueryActive;
                                LOG.debug("Setup jquery active is : "
                                        + currentJqueryActive);
                                return false;
                            }

                            return urlAndTitleCorrect && keyComponentExisted;
                        }

                    });
            testManager
                    .getJQueryDriver()
                    .executeScript(
                            "$.growl.notice({title: \"Page Status\", message: \"The page "
                                    + testedPage.getPageName()
                                    + " is successfully loaded. Browser version is : \"+$.browser.version, location: 'bl', size: 'small'});");

            boolean isWindows = testManager.getJQueryDriver().executeScript(
                    "return $.browser.win;") != null;
            if (isWindows) {
                if (testManager.setupAutoItDriver()) {
                    loadDesktopScript();
                } else {
                    LOG.error("Could not setup Desktop Automation Test. Some action might be failed");
                }
            }

        } catch (Exception e) {
            LOG.error("Exception on setup page", e);
            isfail = true;
            throw new WebActionException("Exception on setup page", e);
        } finally {
            doScreenshot("Load page [" + pageIdentifier + "]", isfail);
        }
    }

    protected void loadDesktopScript() {
        if (testManager.getAutoitDriver() != null) {
            ((JavascriptExecutor) testManager.getAutoitDriver())
                    .executeScript(testManager.getDesktopAutomationScriptPath());
        }
    }

    /**
     * 
     * @param message
     */
    protected void sendNoticeAlert(String message) {
        testManager.getJQueryDriver().executeScript(
                "$.growl.notice({ message: \"" + message
                        + "\", location: 'bl', size: 'small' });");
    }

    /**
     * 
     * @param message
     */
    protected void sendErrorAlert(String message) {
        testManager.getJQueryDriver().executeScript(
                "$.growl.error({ message: \"" + message
                        + "\", location: 'bl', size: 'small' });");
    }

    /**
     * 
     * @param message
     */
    protected void sendWarningAlert(String message) {
        testManager.getJQueryDriver().executeScript(
                "$.growl.warning({ message: \"" + message
                        + "\", location: 'bl', size: 'small' });");
    }

    /**
	 * 
	 */
    protected void loadJSLibrary() {
        JavascriptExecutor executor = testManager.getJQueryDriver();
        // Check visibility library
        executor.executeScript(testManager.getResource("visibility.js"));

        // Checking browser version.
        executor.executeScript(testManager.getResource("jquery.browser.min.js"));

        // Dragdrop for safari
        executor.executeScript(testManager.getResource("jquery.simulate.js"));
        executor.executeScript(testManager
                .getResource("jquery.simulate.ext.js"));
        executor.executeScript(testManager
                .getResource("jquery.simulate.drag-n-drop.js"));

        // Growl plugin for notification
        executor.executeScript(testManager.getResource("jquery.growl.js"));
        String injectedStyle = "$('head').append('<style>"
                + testManager.getResourceSingleLine("jquery.growl.css")
                + "</style>');";
        executor.executeScript(injectedStyle);
    }

    /**
     * 
     * @param webDriver1
     * @param doTitle
     */
    private void changeWindowsIfNecessary(WebDriver webDriver1, boolean doTitle) {
        if (!webDriver1.getCurrentUrl().contains(testedPage.getUrlExtension())) {
            Set<String> windows = webDriver1.getWindowHandles();

            for (String window : windows) {
                webDriver1.switchTo().window(window);
                LOG.info("Changing windows: current window url "
                        + webDriver1.getCurrentUrl());

                if (webDriver1.getCurrentUrl().indexOf(
                        testedPage.getUrlExtension()) > 0) {
                    if (doTitle) {
                        if (webDriver1.getTitle().equals(testedPage.getTitle())) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
        }
    }

    /**
	 * 
	 */
    protected void jqueryActiveChecking() {
        try {
            new FluentWait<>(testManager.getJQueryDriver())
                    .withTimeout(WebUIManager.getElementWaitTimeout(),
                            TimeUnit.MILLISECONDS)
                    .pollingEvery(WebUIManager.getElementPoolingTimeout(),
                            TimeUnit.MILLISECONDS)
                    .ignoring(WebDriverException.class)
                    .until(new Function<JavascriptExecutor, Boolean>() {
                        long lastJqueryActive = 0;

                        @Override
                        public Boolean apply(JavascriptExecutor input) {
                            long currentJqueryActive = (long) testManager
                                    .getJQueryDriver().executeScript(
                                            MessageFormat.format(
                                                    StringRef.RETURN_FORMAT,
                                                    J_QUERY_ACTIVE));
                            if (lastJqueryActive != currentJqueryActive) {
                                lastJqueryActive = currentJqueryActive;
                                LOG.debug("Click jquery active is : "
                                        + currentJqueryActive);
                                return false;
                            }
                            return true;
                        }

                    });
        } catch (TimeoutException e) {
            // silent exception
        }
    }

    @Override
    public boolean doExpect(DomElementExpectation expectation) {

        // Applying action pace delay
        handleWaitAction(WebUIManager.getActionPace());
        jqueryActiveChecking();

        boolean expectationMet = false;

        String condition = expectation.getCondition();

        HtmlActionCondition htmlAction = GenericHtmlActionConditionEnum
                .findValue(condition);
        boolean isFail = false;
        try {
            switch ((GenericHtmlActionConditionEnum) htmlAction) {
            case EXISTS:
                expectationMet = handleExistsAction(expectation);
                break;
            case VISIBLE:
                expectationMet = handleVisibleAction(expectation);
                break;
            case INVISIBLE:
                expectationMet = handleInvisibleAction(expectation);
                break;
            default:
                DomElement domElement = prepareElement(expectation);
                if (domElement == null) {
                    expectationMet = false;
                } else {
                    expectationMet = valueCheck(htmlAction, expectation,
                            domElement);
                }
                break;
            }
        } catch (WebDriverException e) {
            LOG.error("Expect failed", e);
            isFail = true;
        } finally {
            doScreenshot(((GenericHtmlActionConditionEnum) htmlAction).name(),
                    (isFail) ? isFail : !expectationMet);
        }
        return expectationMet;
    }

    /**
     * 
     * @param expectation
     * @return
     */
    private DomElement prepareElement(DomElementExpectation expectation) {
        String componentName = expectation.getComponentName();
        DomElement domElement = testedPage.getDomElement(componentName);
        return domElement;
    }

    /**
     * 
     * @param expectation
     * @return
     */
    private boolean handleExistsAction(DomElementExpectation expectation) {
        boolean result = false;
        DomElement domElement = prepareElement(expectation);
        if (domElement != null && domElement.isExisted()) {
            result = true;
        }
        result = (expectation.isNegative()) ? !result : result;
        LOG.info("Expect " + expectation.getComponentName() + " : is exist ["
                + result + "]");
        return result;
    }

    /**
     * 
     * @param expectation
     * @return
     */
    private boolean handleVisibleAction(DomElementExpectation expectation) {
        boolean result = false;
        DomElement domElement = prepareElement(expectation);
        WebElement webElement = populateElement(domElement);
        result = (webElement != null) && isVisible(webElement);
        result = (expectation.isNegative()) ? !result : result;
        LOG.info("Expect " + expectation.getComponentName() + " : is visible ["
                + result + "]");
        return result;
    }

    /**
     * 
     * @param expectation
     * @return
     */
    private boolean handleInvisibleAction(DomElementExpectation expectation) {
        boolean result = false;
        DomElement domElement = prepareElement(expectation);
        WebElement webElement = domElement.getDomElement();
        result = (webElement == null)
                || !(webElement.isDisplayed() && webElement.isEnabled());
        result = (expectation.isNegative()) ? !result : result;
        LOG.info("Expect " + expectation.getComponentName()
                + " : is invisible [" + result + "]");
        return result;
    }

    /**
     * 
     * @param htmlAction
     * @param expectation
     * @param domElement
     * @return
     */
    protected boolean valueCheck(HtmlActionCondition htmlAction,
            DomElementExpectation expectation, DomElement domElement) {
        String htmlValue = getDomElementHtmlValue(domElement);

        htmlValue = htmlValue.replace(NBSP, WHITESPACE).trim();

        boolean result = false;

        switch ((GenericHtmlActionConditionEnum) htmlAction) {
        case EQUALS:
            result = handleEqualsAction(expectation, htmlValue);
            break;
        case NOT_EQUALS:
            result = handleNotEqualsAction(expectation, htmlValue);
            break;
        default:
            result = handleComparativeAction(expectation, htmlAction, htmlValue);
            break;
        }
        return result;
    }

    /**
     * 
     * @param expectation
     * @param htmlActionConditionEnum
     * @param htmlValue
     * @return
     */
    protected boolean handleComparativeAction(
            DomElementExpectation expectation,
            HtmlActionCondition htmlActionConditionEnum, String htmlValue) {
        boolean result = false;
        double expectDoubleValue = Double.parseDouble((String) expectation
                .getValue());
        String actualValue = htmlValue.replaceAll(_0_9_REGEX, EMPTY_STRING);
        double actualDoubleValue = Double.parseDouble(actualValue);

        switch ((GenericHtmlActionConditionEnum) htmlActionConditionEnum) {
        case GREATER_EQUAL_THAN:
            result = handleGreaterEqualThanAction(expectation,
                    expectDoubleValue, actualDoubleValue);
            break;
        case GREATER_THAN:
            result = handleGreaterThanAction(expectation, expectDoubleValue,
                    actualDoubleValue);
            break;
        case LESS_EQUAL_THAN:
            result = handleLessThanEqualAction(expectation, expectDoubleValue,
                    actualDoubleValue);
            break;
        case LESS_THAN:
            result = handleLessThanAction(expectation, expectDoubleValue,
                    actualDoubleValue);
            break;
        default:
            break;
        }
        return result;
    }

    /**
     * 
     * @param domElement
     * @return
     */
    protected String getDomElementHtmlValue(DomElement domElement) {
        String value = EMPTY_STRING;

        if (domElement.getJquery() != null) {
            LOG.info("find html value jquery: "
                    + MessageFormat.format(StringRef.RETURN_FORMAT,
                            domElement.getJqueryGetHtml()));
            value = (String) testManager.getJQueryDriver().executeScript(
                    MessageFormat.format(StringRef.RETURN_FORMAT,
                            domElement.getJqueryGetHtml()));
        } else {
            value = populateElement(domElement).getAttribute(INNER_HTML);
        }

        return value;
    }

    /**
     * 
     * @param expectation
     * @param htmlValue
     * @return
     */
    protected boolean handleEqualsAction(DomElementExpectation expectation,
            String htmlValue) {
        boolean result = true;
        // if string, we only care if the expected value was contained.
        if (Utils.isANumber(expectation.getValue().toString())) {
            double expectValue = Double.parseDouble(expectation.getValue()
                    .toString());
            double actualValue = Double.parseDouble(htmlValue.replaceAll(
                    _0_9_REGEX, EMPTY_STRING));

            // if they differ by more than a small amount...
            if (Math.abs(expectValue - actualValue) > SMALL_AMOUNT) {
                LOG.error("Expectation failed (expected equals): expects "
                        + expectation.getValue() + "; actual = [" + htmlValue
                        + ']');
                result = false;
            }
        } else {
            if (expectation.isCustomAction()
                    && !htmlValue.contains(expectation.getValue().toString()
                            .trim())) {
                WebElement element = populateElement(prepareElement(expectation));
                try {
                    WebElement kendoElement = populateElement(element,
                            By.xpath("preceding-sibling::*[1]"));
                    if (!kendoElement.getText().contains(
                            expectation.getValue().toString().trim())) {
                        result = false;
                        LOG.error("Expectation failed (expected equals): expects "
                                + expectation.getValue()
                                + "; actual = ["
                                + htmlValue + ']');
                    }
                } catch (NoSuchElementException elementException) {
                    result = false;
                    LOG.error("Expectation failed (expected equals): expects "
                            + expectation.getValue() + "; actual = ["
                            + htmlValue + ']', elementException);
                }
            }
        }
        return result;
    }

    /**
     * 
     * @param expectation
     * @param htmlValue
     * @return
     */
    protected boolean handleNotEqualsAction(DomElementExpectation expectation,
            String htmlValue) {
        boolean result = false;
        if (!expectation.getValue().toString()
                .equalsIgnoreCase(htmlValue.trim())) {
            result = true;
        }
        LOG.info("Expect : " + expectation.getValue() + " not equal with "
                + htmlValue.trim() + " [" + result + "]");

        return result;
    }

    /**
     * 
     * @param expectation
     * @param expectDoubleValue
     * @param actualDoubleValue
     * @return
     */
    private boolean handleGreaterEqualThanAction(
            DomElementExpectation expectation, double expectDoubleValue,
            double actualDoubleValue) {
        boolean result = true;
        if (expectDoubleValue > actualDoubleValue) {
            result = false;
        }
        LOG.info("Expect " + expectation.getComponentName() + " : "
                + expectDoubleValue + " greater equal than "
                + actualDoubleValue + " [" + result + "]");

        return result;
    }

    /**
     * 
     * @param expect
     * @param expectDoubleValue
     * @param actualDoubleValue
     * @return
     */
    private boolean handleLessThanEqualAction(DomElementExpectation expect,
            double expectDoubleValue, double actualDoubleValue) {
        boolean result = true;
        if (expectDoubleValue < actualDoubleValue) {
            result = false;
        }
        LOG.info("Expect " + expect.getComponentName() + " : "
                + expectDoubleValue + " less equal than " + actualDoubleValue
                + " [" + result + "]");

        return result;
    }

    /**
     * 
     * @param expect
     * @param expectDoubleValue
     * @param actualDoubleValue
     * @return
     */
    private boolean handleLessThanAction(DomElementExpectation expect,
            double expectDoubleValue, double actualDoubleValue) {
        boolean result = true;
        if (expectDoubleValue <= actualDoubleValue) {
            result = false;
        }
        LOG.info("Expect " + expect.getComponentName() + " : "
                + expectDoubleValue + " less than " + actualDoubleValue + " ["
                + result + "]");

        return result;
    }

    /**
     * 
     * @param expectation
     * @param expectDoubleValue
     * @param actualDoubleValue
     * @return
     */
    private boolean handleGreaterThanAction(DomElementExpectation expectation,
            double expectDoubleValue, double actualDoubleValue) {
        boolean result = true;
        if (expectDoubleValue >= actualDoubleValue) {
            result = false;
        }
        LOG.info("Expect " + expectation.getComponentName() + " : "
                + expectDoubleValue + " greater than " + actualDoubleValue
                + " [" + result + "]");
        return result;
    }

    String alertMesageFormat = "Start executing %s on %s";

    @Override
    public HtmlActionStatus doAction(ActionRequest actionRequest)
            throws WebActionException {
        // Applying action pace delay
        handleWaitAction(WebUIManager.getActionPace());
        jqueryActiveChecking();

        // Non element action
        HtmlAction action = (GenericHtmlAction) actionRequest.getAction();
        HtmlActionStatus nonElementActionResult = handleNonElementAction(actionRequest,
                action);
        if(!nonElementActionResult.equals(HtmlActionStatus.NOT_SUPPORTED)){
            return nonElementActionResult;
        }

        // Element action
        WebElement element = populateElement(actionRequest
                .getElementIdentifier());

        // get condition
        boolean fail = false;
        try {
            HtmlActionStatus result = handleAction(element, action,
                    actionRequest.getParameters());
            if (result == HtmlActionStatus.NOT_SUPPORTED) {
                result = customAction(actionRequest);
            }
            return result;
        } catch (WebActionException e) {
            LOG.error("Exception on executing action", e);
            fail = true;
            throw new WebActionException("Exception on executing action", e);
        } finally {
            doScreenshot(((GenericHtmlAction) action).name(), fail);
        }
    }

    protected HtmlActionStatus handleNonElementAction(
            ActionRequest actionRequest, HtmlAction action)
            throws WebActionException {
        if (action == GenericHtmlAction.WAIT) {
            handleWaitAction((int) actionRequest.getParameter(0));
            return HtmlActionStatus.GOOD;
        } else if (action == GenericHtmlAction.REFRESH) {
            handleRefreshAction();
            return HtmlActionStatus.GOOD;
        } else if (action == GenericHtmlAction.CHOOSE_FILE) {
            String filePath = (String) actionRequest.getParameter(0);
            String textFieldId = (String) actionRequest.getParameter(1);
            String buttonId = (String) actionRequest.getParameter(2);
            return handleChooseFileAction(filePath, textFieldId, buttonId);
        }
        return HtmlActionStatus.NOT_SUPPORTED;
    }

    /**
     * Check configuration on how the system take screenshot
     * 
     * @param screenshotName
     * @param fail
     */
    protected void doScreenshot(String screenshotName, boolean fail) {
        switch (testManager.getScreenshotMode()) {
        case ALL:
            takeScreenshot(screenshotName);
            break;
        case ERROR_ONLY:
            if (fail) {
                takeScreenshot(screenshotName);
            }
            break;
        default:
            break;
        }
    }

    /**
     * Take screenshot and embed the screenshot in scenario
     * 
     * @param stepName
     */
    private void takeScreenshot(String stepName) {
        byte[] screenshotInBytes = ((TakesScreenshot) testManager
                .getScreenshotDriver()).getScreenshotAs(OutputType.BYTES);
        // TODO: move this to after step hook cucumber if it is available in the
        // future
        scenario.embed(screenshotInBytes, "image/png");
    }

    protected HtmlActionStatus customAction(ActionRequest actionRequest)
            throws WebActionException {
        return null;
    }

    /**
     * Generic do Action to execute action at element
     * 
     * @param element
     * @param action
     * @param params
     * @return
     * @throws WebActionException
     */
    protected HtmlActionStatus handleAction(WebElement element,
            HtmlAction action, List<Object> params) throws WebActionException {
        try {
            switch ((GenericHtmlAction) action) {
            case CLICK:
                handleClickAction(element);
                break;
            case HOVER:
                handleHoverAction(element);
                break;
            case ENTER:
                handleEnterAction(element, (String) params.get(0));
                break;
            case DOUBLE_CLICK:
                handleDoubleClickAction(element);
                break;
            case DRAG:
                handleDragAction(element, (String) params.get(0));
                break;
            case SELECT:
                handleSelectAction(element, (String) params.get(0));
                break;
            default:
                return HtmlActionStatus.NOT_SUPPORTED;
            }
            return HtmlActionStatus.GOOD;
        } catch (WebActionException e) {
            LOG.error("Exception on executing action", e);
            throw new WebActionException("Exception on executing action", e);
        }
    }

    /**
     * Handle select action
     * 
     * @param element
     * @param target
     */
    protected void handleSelectAction(WebElement element, String target) {
        Select select = new Select(element);
        select.selectByVisibleText(target);
    }

    /**
     * Handle drag action
     * 
     * @param element
     * @param target
     */
    protected void handleDragAction(WebElement element, final String target) {
        moveToElement(element);
        WebElement targetPosition = populateElement(target);
        handleDragAction(element, targetPosition);
    }

    /**
     * Handle drag action
     * 
     * @param origin
     * @param target
     */
    protected void handleDragAction(WebElement origin, final WebElement target) {
        getWebElementLoader(origin).until(new Function<WebElement, Boolean>() {

            long lastJqueryActive = 0;

            @Override
            public Boolean apply(WebElement originPosition) {
                long currentJqueryActive = (long) testManager.getJQueryDriver()
                        .executeScript("return jQuery.active");
                if (lastJqueryActive != currentJqueryActive) {
                    lastJqueryActive = currentJqueryActive;
                    LOG.debug("drag jquery active is : " + currentJqueryActive);
                    return false;
                }
                if (testManager.getBrowser().equals(Browsers.Safari)) {
                    dragdrop(originPosition, target);
                } else {
                    Actions actions = new Actions(testManager.getWebDriver());
                    actions.dragAndDrop(originPosition, target).build()
                            .perform();
                    handleWaitAction(WebUIManager.getElementPoolingTimeout());
                }
                handleWaitAction(1000);
                return true;
            }
        });
    }

    /**
     * Handle drag and drop action
     * 
     * @param origin
     * @param target
     */
    public void dragdrop(WebElement origin, WebElement target) {
        testManager.getJQueryDriver().executeScript(
                "var target = $(arguments[1]); $(" + ARGUMENTS_0
                        + ").simulate('drag-n-drop',{dragTarget: target})",
                origin, target);
    }

    /**
     * Handle double click action
     * 
     * @param element
     */
    protected void handleDoubleClickAction(WebElement element) {
        getWebElementLoader(element).until(new Function<WebElement, Boolean>() {

            long lastJqueryActive = 0;

            @Override
            public Boolean apply(WebElement input) {
                long currentJqueryActive = (long) testManager.getJQueryDriver()
                        .executeScript("return jQuery.active");
                LOG.debug("Double click jquery active is : "
                        + currentJqueryActive);
                if (lastJqueryActive != currentJqueryActive) {
                    lastJqueryActive = currentJqueryActive;
                    LOG.debug("Double click jquery active is : "
                            + currentJqueryActive);
                    return false;
                }
                moveToElement(input);
                if (testManager.getBrowser().equals(Browsers.Safari)) {
                    safariDoubleClick(input);
                } else {
                    Actions webActions = new Actions(testManager.getWebDriver());
                    webActions.moveToElement(populateElement(input, self))
                            .doubleClick().build().perform();
                }
                return true;
            }
        });
    }

    /**
     * Handling double click method in safari
     * 
     * @param element
     */
    private void safariDoubleClick(WebElement element) {
        testManager
                .getJQueryDriver()
                .executeScript(
                        "var e = $.Event('dblclick'); e.pageX = "
                                + ARGUMENTS_0
                                + "; e.pageY = arguments[1]; $(arguments[2]).trigger(e)",
                        element.getLocation().x, element.getLocation().y,
                        element);
    }

    /**
     * Handle enter statement
     * 
     * @param element
     * @param value
     * @throws WebActionException
     */
    protected void handleEnterAction(WebElement element, String value)
            throws WebActionException {
        handleEnterAction(element, value, true);
    }

    protected void handleEnterAction(WebElement element, String value,
            boolean requestFocus) throws WebActionException {
        if (requestFocus) {
            handleClickAction(element);
        }
        if (value.startsWith("Keys.")) {
            String keys = value.replace("Keys.", "");
            element.sendKeys(Keys.valueOf(keys));
        } else {
            element.clear();
            element.sendKeys(value);
        }

        testManager.getJQueryDriver().executeScript(
                "$(arguments[0]).change(); return true", element);
    }

    protected static final String AUTOIT_SPECIAL_ACTION = "{%s}";

    protected void handleAutoItEnterAction(WebElement element,
            final String value) throws WebActionException {

        getWebElementLoader(element).until(new Function<WebElement, Boolean>() {

            @Override
            public Boolean apply(WebElement input) {
                if (value.startsWith("Keys.")) {
                    // convert special action to autoit format
                    String keys = value.replace("Keys.", "");
                    input.sendKeys(String.format(AUTOIT_SPECIAL_ACTION, keys));
                } else {
                    input.sendKeys(value);
                }
                return true;
            }

        });
    }

    protected HtmlActionStatus handleChooseFileAction(String filePath,
            String fieldId, String buttonId) throws WebActionException {
        // Wait for upload dialog is appear
        handleWaitAction(WebUIManager.getElementContentTimeout());
        if (!testManager.getBrowser().equals(Browsers.Safari)) {
            try {
                String windowTitle = getUploadDialogTitle(testManager
                        .getBrowser());
                testManager.getAutoitDriver().switchTo().window(windowTitle);
                WebElement fieldElement = populateAutoItElement(testedPage
                        .getDomElement(fieldId));
                handleAutoItEnterAction(fieldElement, filePath);
                WebElement buttonElement = populateAutoItElement(testedPage
                        .getDomElement(buttonId));
                handleAutoItClickAction(buttonElement);
            } catch (NullPointerException e) {
                throw new WebActionException(
                        "Desktop Automation not configured.");
            }
        } else {
            safariUpload(filePath);
        }
        return HtmlActionStatus.GOOD;
    }

    protected void handleAutoItClickAction(WebElement element) {
        getWebElementLoader(element).until(new Function<WebElement, Boolean>() {

            @Override
            public Boolean apply(WebElement input) {
                input.click();
                return true;
            }

        });
    }

    protected String getUploadDialogTitle(Browsers browsers) {
        switch (browsers) {
        case IE:
            return "Choose File to Upload";
        case Firefox:
            return "File Upload";
        case Chrome:
            return "Open";
        default:
            return "";
        }
    }

    protected void safariUpload(String pathFile) throws WebActionException {
        // TODO: Implement safari upload mechanism
        throw new WebActionException(
                "Upload file for safari not implemented yet");
    }

    /**
     * Wait action
     * 
     * @param duration
     *            in milliseconds
     */
    protected void handleWaitAction(int duration) {
        try {
            Thread.sleep(duration);
        } catch (InterruptedException e) {
            LOG.error("Wait is interrupted", e);
        }
    }

    /**
     * Refresh the page.
     * 
     * @throws WebActionException
     */
    protected void handleRefreshAction() throws WebActionException {
        testManager.getWebDriver().navigate().refresh();
        setup(testedPage.getPageName());
    }

    /**
     * Load element from Page.domElements
     * 
     * @param identifier
     * @return
     */
    protected WebElement populateElement(String identifier) {
        DomElement element = testedPage.getDomElement(identifier);
        return populateElement(element);
    }

    /**
     * Load element from Page.domElements
     * 
     * @param identifier
     * @return
     */
    protected List<WebElement> populateElements(String identifier) {
        DomElement element = testedPage.getDomElement(identifier);
        return populateElements(element);
    }

    /**
     * Populate element directly from domElement.
     * 
     * @param element
     * @return
     */
    protected List<WebElement> populateElements(DomElement element) {
        if (element.getId() != null) {
            return populateElements(By.id(element.getId()));
        } else if (element.getName() != null) {
            return populateElements(By.name(element.getName()));
        } else if (element.getLinkText() != null) {
            return populateElements(By.linkText(element.getLinkText()));
        } else if (element.getClassName() != null) {
            return populateElements(By.className(element.getClassName()));
        } else if (element.getXpath() != null) {
            return populateElements(By.xpath(element.getXpath()));
        } else if (element.getCssSelector() != null) {
            return populateElements(By.cssSelector(element.getCssSelector()));
        } else if (element.getPartialLinkText() != null) {
            return populateElements(By.partialLinkText(element
                    .getPartialLinkText()));
        } else if (element.getTagName() != null) {
            return populateElements(By.tagName(element.getTagName()));
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * Populate element directly from domElement.
     * 
     * @param element
     * @return
     */
    protected WebElement populateElement(DomElement element) {
        if (element.getId() != null) {
            return populateElement(By.id(element.getId()));
        } else if (element.getName() != null) {
            return populateElement(By.name(element.getName()));
        } else if (element.getLinkText() != null) {
            return populateElement(By.linkText(element.getLinkText()));
        } else if (element.getClassName() != null) {
            return populateElement(By.className(element.getClassName()));
        } else if (element.getXpath() != null) {
            return populateElement(By.xpath(element.getXpath()));
        } else if (element.getCssSelector() != null) {
            return populateElement(By.cssSelector(element.getCssSelector()));
        } else if (element.getPartialLinkText() != null) {
            return populateElement(By.partialLinkText(element
                    .getPartialLinkText()));
        } else if (element.getTagName() != null) {
            return populateElement(By.tagName(element.getTagName()));
        } else {
            return null;
        }
    }

    /**
     * Terminate test. Close browser and kill the process
     */
    public void terminateProcessor() {
        testManager.terminateWebDriver();
    }

    /**
     * Populate Element to avoiding {@link StaleElementReferenceException}
     * 
     * @param by
     * @return
     */
    protected WebElement populateElement(By by) {
        return populateElement(null, by);
    }

    /**
     * Populate Element to avoiding {@link StaleElementReferenceException} with
     * root element
     * 
     * @param element
     *            element root of element
     * @param by
     *            query of element based on root
     * @return
     */
    protected WebElement populateElement(WebElement element, By by) {
        return populateClickableElement(element, by, false);
    }

    /**
     * Populate element and make sure the element is clickable
     * 
     * @param webElement
     * @param by
     * @param clickable
     * @return
     */
    protected WebElement populateClickableElement(final WebElement webElement,
            By by, final boolean clickable) {
        return getByLoader(by).until(new Function<By, WebElement>() {
            @Override
            public WebElement apply(By input) {
                WebElement result = null;
                if (!testManager.getBrowser().equals(Browsers.Firefox)) {
                    result = (webElement != null) ? webElement
                            .findElement(input) : testManager.getWebDriver()
                            .findElement(input);
                } else {
                    List<WebElement> elements = (webElement != null) ? populateElements(
                            webElement, input) : populateElements(input);
                    if (!elements.isEmpty()) {
                        result = elements.get(0);
                    } else {
                        throw new NoSuchElementException("No Element Found");
                    }
                }
                if (!clickable) {
                    return result;
                }
                return new WebDriverWait(testManager.getWebDriver(),
                        (long) TimeUnit.SECONDS.convert(
                                WebUIManager.getElementWaitTimeout(),
                                TimeUnit.MILLISECONDS))
                        .ignoring(NoSuchElementException.class)
                        .ignoring(StaleElementReferenceException.class)
                        .until(ExpectedConditions.elementToBeClickable(result));
            }
        });
    }

    protected WebElement populateAutoItElement(DomElement domElement) {
        if (domElement.getId() != null) {
            return populateAutoItElement(By.id(domElement.getId()));
        } else if (domElement.getName() != null) {
            return populateAutoItElement(By.name(domElement.getName()));
        } else if (domElement.getLinkText() != null) {
            return populateAutoItElement(By.linkText(domElement.getLinkText()));
        } else if (domElement.getClassName() != null) {
            return populateAutoItElement(By
                    .className(domElement.getClassName()));
        } else if (domElement.getXpath() != null) {
            return populateAutoItElement(By.xpath(domElement.getXpath()));
        } else if (domElement.getCssSelector() != null) {
            return populateAutoItElement(By.cssSelector(domElement
                    .getCssSelector()));
        } else if (domElement.getPartialLinkText() != null) {
            return populateAutoItElement(By.partialLinkText(domElement
                    .getPartialLinkText()));
        } else if (domElement.getTagName() != null) {
            return populateAutoItElement(By.tagName(domElement.getTagName()));
        } else {
            return null;
        }
    }

    protected WebElement populateAutoItElement(By by) {
        return getByLoader(by).until(new Function<By, WebElement>() {

            @Override
            public WebElement apply(By input) {
                return testManager.getAutoitDriver().findElement(input);
            }

        });
    }

    /**
     * Populate Element to avoiding {@link StaleElementReferenceException}
     * 
     * @param by
     * @return
     */
    protected List<WebElement> populateElements(By by) {
        return populateElements(null, by);
    }

    /**
     * Populate Element to avoiding {@link StaleElementReferenceException} with
     * root element
     * 
     * @param element
     *            root of element
     * @param by
     *            query of element based on root
     * @return
     */
    protected List<WebElement> populateElements(WebElement element, By by) {
        return populateElements(element, by, false);
    }

    /**
     * Populate list of elements and wait until the timeout
     * 
     * @param element
     * @param by
     * @param allVisible
     * @return
     */
    protected List<WebElement> populateElements(final WebElement element,
            By by, final boolean allVisible) {
        return populateElements(element, by, allVisible, 0);
    }

    /**
     * Populate list of elements and wait until the timeout
     * 
     * @param element
     * @param by
     * @param allVisible
     * @param timeout
     * @return
     */
    protected List<WebElement> populateElements(final WebElement element,
            By by, final boolean allVisible, final long timeout) {
        return getByLoader(by).until(new Function<By, List<WebElement>>() {

            @Override
            public List<WebElement> apply(By input) {

                List<WebElement> result = (element != null) ? element
                        .findElements(input) : testManager.getWebDriver()
                        .findElements(input);
                if (!allVisible) {
                    return result;
                } else {
                    List<WebElement> check = new WebDriverWait(testManager
                            .getWebDriver(), TimeUnit.SECONDS.convert(
                            (timeout > 0) ? timeout : WebUIManager
                                    .getElementWaitTimeout(),
                            TimeUnit.MILLISECONDS)).until(ExpectedConditions
                            .visibilityOfAllElements(result));
                    for (WebElement element : check) {
                        if (!isVisible(element)) {
                            break;
                        }
                        LOG.info(element + " is visible");
                    }
                    return check;
                }
            }

        });
    }

    /**
     * Handling click action
     * 
     * @param element
     * @throws WebActionException
     */
    protected void handleClickAction(WebElement element)
            throws WebActionException {
        handleClickAction(element, false);
    }

    /**
     * Get loader for web element
     * 
     * @param element
     * @return
     */
    private Wait<WebElement> getWebElementLoader(WebElement element) {
        return new FluentWait<WebElement>(element)
                .withTimeout((long) WebUIManager.getElementWaitTimeout(),
                        TimeUnit.MILLISECONDS)
                .pollingEvery(WebUIManager.getElementPoolingTimeout(),
                        TimeUnit.MILLISECONDS)
                .ignoring(WebDriverException.class);
    }

    /**
     * Get loader for By
     * 
     * @param by
     * @return
     */
    private Wait<By> getByLoader(By by) {
        return new FluentWait<By>(by)
                .withTimeout((long) WebUIManager.getElementWaitTimeout(),
                        TimeUnit.MILLISECONDS)
                .pollingEvery(WebUIManager.getElementPoolingTimeout(),
                        TimeUnit.MILLISECONDS)
                .ignoring(WebDriverException.class);
    }

    /**
     * Handling click action in webdriver
     * 
     * @param element
     * @param useJQuery
     * @throws WebActionException
     */
    protected void handleClickAction(WebElement element, final boolean useJQuery)
            throws WebActionException {
        try {
            if (!useJQuery) {
                moveToElement(element);
            }
            getWebElementLoader(element).until(
                    new Function<WebElement, Boolean>() {

                        long lastJqueryActive = 0;
                        int retry = 0;

                        @Override
                        public Boolean apply(WebElement input) {
                            retry++;
                            LOG.debug("click retry : " + retry);
                            long currentJqueryActive = (long) testManager
                                    .getJQueryDriver().executeScript(
                                            "return jQuery.active");
                            if (lastJqueryActive != currentJqueryActive) {
                                lastJqueryActive = currentJqueryActive;
                                LOG.debug("Click jquery active is : "
                                        + currentJqueryActive);
                                return false;
                            }
                            if (useJQuery) {
                                LOG.info("clicked by jquery");
                                String clickCommand = EMPTY_STRING;
                                if (testManager.getBrowser()
                                        .equals(Browsers.IE)) {
                                    clickCommand = EMPTY_STRING + ARGUMENTS_0
                                            + ".click()";
                                } else if (testManager.getBrowser().equals(
                                        Browsers.Safari)) {
                                    clickCommand = "$(" + ARGUMENTS_0
                                            + ").click()";
                                }
                                testManager.getJQueryDriver().executeScript(
                                        clickCommand, input);
                            } else {
                                input.click();
                            }
                            return true;
                        }
                    });
        } catch (WebDriverException wde) {
            LOG.error("Failed to click", wde);
            throw new WebActionException("Error executing click event.", wde);
        }
    }

    /**
     * Moving screen to correspond Element.
     * 
     * @param element
     */
    protected void moveToElement(WebElement element) {
        LOG.debug("start " + element + " visibility : " + isVisible(element));
        if (!isVisible(element)) {

            moveTo(element, false);

        }
        LOG.debug("End " + element + " visibility : " + isVisible(element));
    }

    /**
     * Make the webdriver to move to the element
     * 
     * @param element
     * @param customContainer
     */
    protected void moveTo(WebElement element, boolean customContainer) {
        int scrollPadding = 30;
        if (!testManager.getBrowser().equals(Browsers.Safari)) {
            // Unknown command in safari
            try {

                if (!isVisible(element) && !customContainer) {
                    // in case move to element not changing scroll position
                    testManager.getJQueryDriver().executeScript(
                            EMPTY_STRING + ARGUMENTS_0
                                    + ".scrollIntoView(true);", element);
                }

            } catch (MoveTargetOutOfBoundsException e) {
                LOG.error("Element isn't at screen");
            }
        } else if (!customContainer) {
            long heightOfScreen = (long) testManager.getJQueryDriver()
                    .executeScript(
                            MessageFormat.format(StringRef.RETURN_FORMAT,
                                    $_WINDOW_HEIGHT));
            int movement = (int) ((element.getLocation().y + element.getSize().height) - heightOfScreen);
            LOG.debug("movement : " + movement);
            testManager.getJQueryDriver()
                    .executeScript(
                            "window.scrollTo(" + ARGUMENTS_0 + ","
                                    + ARGUMENTS_1 + ");", 0,
                            Math.abs(movement) + scrollPadding);
            LOG.debug(element + " now visible [" + isVisible(element) + "]");
        }
    }

    /**
     * Checking visibility of element in the screen using custom javascript
     * method as the current isVisible method from selenium could not detect
     * partial display of element
     * 
     * @param element
     * @return whether element visible on screen or not
     */
    protected boolean isVisible(WebElement element) {
        boolean inViewport = (boolean) testManager.getJQueryDriver()
                .executeScript(testManager.getResource("inViewport.js"),
                        element);
        JavascriptExecutor executor = testManager.getJQueryDriver();
        boolean result = (boolean) executor.executeScript(MessageFormat.format(
                StringRef.RETURN_FORMAT, ARGUMENTS_0_IS_VISIBLE_ARGUMENTS_0),
                element);
        LOG.debug("checking " + element + " : " + result);
        return result && inViewport;

    }

    /**
     * Do custom scroll according to framework, with element reference
     * 
     * @param element
     */
    protected void customScroll(WebElement element) {

    }

    /**
     * Hover action
     * 
     * @param element
     */
    protected void handleHoverAction(WebElement element) {
        getWebElementLoader(element).until(new Function<WebElement, Boolean>() {

            long lastJqueryActive = 0;

            @Override
            public Boolean apply(WebElement input) {
                long currentJqueryActive = (long) testManager.getJQueryDriver()
                        .executeScript(
                                MessageFormat.format(StringRef.RETURN_FORMAT,
                                        J_QUERY_ACTIVE));
                if (lastJqueryActive != currentJqueryActive) {
                    lastJqueryActive = currentJqueryActive;
                    LOG.debug("Hover jquery active is : " + currentJqueryActive);
                    return false;
                }
                if (testManager.getBrowser().equals(Browsers.Safari)) {
                    safariHover(input);
                } else {
                    Actions actions = new Actions(testManager.getWebDriver());
                    actions.moveToElement(populateElement(input, self))
                            .perform();
                }

                currentJqueryActive = (long) testManager.getJQueryDriver()
                        .executeScript(
                                MessageFormat.format(StringRef.RETURN_FORMAT,
                                        J_QUERY_ACTIVE));
                if (lastJqueryActive != currentJqueryActive) {
                    lastJqueryActive = currentJqueryActive;
                    LOG.debug("Hover jquery active is : " + currentJqueryActive);
                    return false;
                }
                return true;
            }
        });
    }

    /**
     * Hovering method for safari since it is different than other
     * 
     * @param element
     */
    public void safariHover(WebElement element) {
        String hoverScript = "var evt = $.Event(\"mouseover\"); $("
                + ARGUMENTS_0 + ").trigger(evt);";
        testManager.getJQueryDriver().executeScript(hoverScript, element);
    }

    private static GenericActionProcessor mInstance;

    /**
     * instantiate the action processor
     * 
     * @return
     */
    public static synchronized GenericActionProcessor getInstance() {
        if (mInstance == null) {
            mInstance = new GenericActionProcessor();
        }
        return mInstance;
    }
}
