package com.mitrais.guitestautomation.processor;

import com.mitrais.guitestautomation.action.HtmlActionStatus;
import com.mitrais.guitestautomation.exception.WebActionException;
import com.mitrais.guitestautomation.page.DomElementExpectation;

import cucumber.api.Scenario;

public interface ActionProcessor {
    HtmlActionStatus doAction(ActionRequest actionRequest)
            throws WebActionException;

    boolean doExpect(DomElementExpectation expectation);

	void setup(String arg1) throws WebActionException;

	void setScenario(Scenario scenario);

	void terminateProcessor();
    
}
