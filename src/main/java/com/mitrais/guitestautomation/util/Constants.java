package com.mitrais.guitestautomation.util;

public class Constants {
    // mitrais.properties
    public static final String MITRAIS = "mitrais";
    public static final String PROJECT_NAME = "projectName";
    public static final String USE_REMOTE = "useRemote";
    public static final String REMOTE_WEB_DRIVER_URL = "remoteWebDriverUrl";
    public static final String SAFARI_PADDING_TOP = "safariPaddingTop";
    public static final String SAFARI_PADDING_LEFT = "safariPaddingLeft";
    public static final String SETTINGS_USE_XML = "useXml";
    public static final String PAGES_DIRECTORY_NAME = "pagesDirectoryName";
    public static final String PAGE_LOAD_TIMEOUT = "pageLoadTimeout";
    public static final String ACTION_PACE = "actionPace";
    public static final String FEATURE_NAMES_EXECUTION = "features";
    public static final String CUSTOM_CLASSPATH = "customClasspath";
    public static final String BASE_URL = "baseUrl";
    public static final String SCREENSHOT_MODE = "screenshotMode";
    public static final String ELEMENT_PACE = "elementPace";
    public static final String ELEMENT_WAIT_TIMEOUT = "elementWaitTimeout";
    public static final String ELEMENT_POOLING_TIMEOUT = "elementPoolingTimeout";
    public static final String ELEMENT_CONTENT_TIMEOUT = "elementContentTimeout";
    public static final String DESKTOP_AUTOMATION_SERVER_PORT = "desktopAutomationServerPort";
    public static final String DESKTOP_AUTOMATION_SCRIPT = "desktopAutomationScript";

    // mitrais.properties optional:
    public static final String BROWSER = "browser";
    public static final String PLATFORM = "platform";
    public static final String BROWSER_VERSION = "browserVersion";
    public static final String CHROME_CONFIGURATION="chrome.";
    public static final String OPERA_CONFIGURATION="opera.";
    public static final String IE_CONFIGURATION="ie.";
    public static final String BROWSER_SIZE= "browserSize";

    // defaults
    public static final String DEFAULT_BROWSER = "firefox";

    // others
    public static final String DASH = "-";
    public static final String COMMA = ",";
    public static final String COLON = ":";
    public static final String LESS_THAN = "<";
    public static final String GREATER_THAN = ">";
    public static final String DOT = "\\.";
    public static final String SLASH = "/";
    public static final String BACK_SLASH = "\\";
    public static final String DOUBLE_SLASH = "//";
    public static final String DOUBLE_PIPE = "\\|\\|";
    public static final String DOUBLE_AND = "&&";
    public static final String SEMICOLON = ";";
    public static final String LEFT_SQUARE_BRACKET = "[";
    public static final String RIGHT_SQUARE_BRACKET = "]";
    public static final String LEFT_PARENTHESIS = "\\(";
    public static final String RIGHT_PARENTHESIS = "\\)";
    public static final String EXCLAMATION_MARK = "!";
    public static final String DATA_FOLDER_NAME = "data";
    public static final String IMG_FOLDER_NAME = "img";
    public static final String REPORT_JS_PRE_JSON = "result =";
    public static final String DATA_JS_PRE_JSON = "data =";
    public static final String TEMPLATES_FOLDER_NAME = "templates";
    public static final String JS_LIB_FOLDER = "jslib";
    public static final String[] JS_LIB_FILES = { "backbone.js",
            "backbone-relational.js", "jquery.js", "underscore.js" };
    public static final String[] SUPPORTED_BROWSERS = { "ie", "firefox",
            "chrome", "safari" };
    public static final String CUSTOM_ACTION_INDICATOR = "$action=";
    public static int STEP_NUMBER = 1;
	public static final double SMALL_AMOUNT = 0.00001d;
	public static final String _0_9_REGEX = "[^0-9\\.]";
	public static final String INNER_HTML = "innerHTML";
	public static final String $_WINDOW_HEIGHT = "$(window).height()";
	public static final String DOCUMENT_READY_STATE = "document.readyState";
	public static final String ARGUMENTS_0 = "arguments[0]";
	public static final String ARGUMENTS_1 = "arguments[1]";
	public static final String J_QUERY_ACTIVE = "jQuery.active";
	public static final String COMPLETE = "complete";
	public static final String EMPTY_STRING = "";
	public static final String WHITESPACE = " ";
	public static final String NBSP = "&nbsp;";
    public static final String DESKTOP_SERVER_ENDPOINT_PATH = "/wd/hub";
    private Constants() {
    }
    public static class StringRef{
    	public static final String RETURN_FORMAT = "return {0}";
    }
    
}
