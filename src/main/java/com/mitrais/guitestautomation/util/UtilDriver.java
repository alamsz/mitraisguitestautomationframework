package com.mitrais.guitestautomation.util;

public class UtilDriver {

    public static void main(String[] args) {
        String savePath = args[0];
        Utils.generateJsonFromKeyword(savePath);
        System.exit(0);
    }

}
