package com.mitrais.guitestautomation.util;

/** Created by douglas_bullard on 1/20/14. */
public enum Drivers {
    CHROME("drivers/chromedriver", "drivers/chromedriver.exe"), IE(null,
            "drivers/IEDriverServer.exe"), OPERA(null, null), NONE(null, null);

    private final String macDriverName;
    private final String windowsDriverName;

    Drivers(String macDriverName, String windowsDriverName) {
        this.macDriverName = macDriverName;
        this.windowsDriverName = windowsDriverName;
    }

    public String getDriverName() {
        if (Os.getOs() == Os.WINDOWS) {
            return windowsDriverName;
        }

        if (Os.getOs() == Os.OS_X) {
            return macDriverName;
        } else {
            return null; // nothing else supported
        }
    }
}
