package com.mitrais.guitestautomation.util;

public enum ScreenshotMode {
    ALL, ERROR_ONLY, NONE
}
