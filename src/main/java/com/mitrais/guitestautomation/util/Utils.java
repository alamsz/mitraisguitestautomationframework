package com.mitrais.guitestautomation.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.mitrais.guitestautomation.exception.IllegalCucumberFormatException;
import com.mitrais.guitestautomation.page.DomElementExpectation;
import com.mitrais.guitestautomation.page.GenericHtmlActionConditionEnum;

import cucumber.api.DataTable;

public class Utils {
    private Utils() {
    }

    // jsheridan CODEREVIEW - why not use number utils? or else rename isADouble
    public static boolean isANumber(String aString) {
        try {
            Double.parseDouble(aString);

            return true;
        } catch (NumberFormatException ne) {
            return false;
        }
    }

    public static void generateJsonFromKeyword(String savePath) {
        try {
            Class c = Class
                    .forName("com.mitrais.guitestautomation.bridgetest.BridgeStepDefinition");
            JsonObject fullObject = new JsonObject();
            for (Method method : c.getMethods()) {
                Annotation[] annotations = method.getAnnotations();
                for (Annotation annotation : annotations) {
                    if (annotation.annotationType().getSimpleName()
                            .equals("Given")
                            || annotation.annotationType().getSimpleName()
                                    .equals("When")
                            || annotation.annotationType().getSimpleName()
                                    .equals("Then")) {
                        String value = annotation.annotationType()
                                .getMethod("value")
                                .invoke(annotation, (Object[]) null).toString();
                        JsonObject object = new JsonObject();
                        String regex = "[\\().*?^$\"]|(d\\+)";
                        String realValue = value.replaceAll(regex, "");
                        object.addProperty("Name", value);
                        fullObject.add(realValue, object);
                    }
                }
            }
            String keywordFileName = "keywords.json";
            Gson gson = new Gson();
            String jsonString = gson.toJson(fullObject);
            FileWriter writer = new FileWriter(keywordFileName);
            writer.write(jsonString);
            writer.close();
            File keywordFile = new File(keywordFileName);
            File savePathFolder = new File(savePath);
            FileUtils.deleteDirectory(savePathFolder);
            savePathFolder.mkdir();
            FileUtils.moveFileToDirectory(keywordFile, savePathFolder, false);
        } catch (ClassNotFoundException | NoSuchMethodException
                | SecurityException | IllegalAccessException
                | IllegalArgumentException | InvocationTargetException
                | JsonIOException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    
    /**
     * 
     * @param identifier
     * @return
     */
    public static String normalizeIdentifier(String identifier) {
        if (identifier.contains(".")) {
            return identifier.substring(identifier.indexOf(".") + 1);
        } else {
            return identifier;
        }
    }
    
    /**
     * Generate Expect object to be evaluated.
     * 
     * @param dataTable
     * @return
     * @throws IllegalCucumberFormatException
     */
    public static List<DomElementExpectation> expectBridge(DataTable dataTable)
            throws IllegalCucumberFormatException {
        List<DomElementExpectation> expectations = new ArrayList<DomElementExpectation>();
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        for (String dataKey : data.keySet()) {
            String expectation = data.get(dataKey);
            DomElementExpectation expect = new DomElementExpectation();

            expect.setComponentName(Utils.normalizeIdentifier(dataKey));
            // Jazz
            if (expectation.equalsIgnoreCase(GenericHtmlActionConditionEnum.VISIBLE
                    .getValue())) {
                expect.setCondition(GenericHtmlActionConditionEnum.VISIBLE.getValue());
            } else if (expectation
                    .equalsIgnoreCase(GenericHtmlActionConditionEnum.INVISIBLE
                            .getValue())) {
                expect.setCondition(GenericHtmlActionConditionEnum.INVISIBLE
                        .getValue());
            } else {
                expect.setCondition(GenericHtmlActionConditionEnum.EQUALS.getValue());
                expect.setValue(expectation);
            }
            expectations.add(expect);
        }
        return expectations;
    }

}
