package com.mitrais.guitestautomation.page;

public class HtmlCondition {
    private String componentName;
    private GenericHtmlActionConditionEnum condition;

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public GenericHtmlActionConditionEnum getCondition() {
        return condition;
    }

    public void setCondition(GenericHtmlActionConditionEnum condition) {
        this.condition = condition;
    }
}
