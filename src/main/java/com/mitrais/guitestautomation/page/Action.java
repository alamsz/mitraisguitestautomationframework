package com.mitrais.guitestautomation.page;

import com.mitrais.guitestautomation.exception.WebActionException;

/**
 * Custom action interface
 */
public interface Action {
    String fire() throws WebActionException;
}
