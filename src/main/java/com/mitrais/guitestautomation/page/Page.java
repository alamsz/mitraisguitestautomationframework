package com.mitrais.guitestautomation.page;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mitrais.guitestautomation.controller.WebUIManager;

/**
 * This is the Java Page Object representing a html page object. After
 * setWebDriver and setJsDriver, one must performing setup() to load page. To do
 * navigation from one this page to a new page, gotoPage needs to be called.
 * Typical sequences are:
 */
@XmlRootElement
public class Page {
    private static Logger LOG = LoggerFactory.getLogger(Page.class);
    private String pageName;
    private String title;
    private String urlExtension;
    private Map<String, DomElement> domElements = new HashMap<>();

    // a key component must exist before a page can be operational.
    private String keyDomElementName = null;

    public DomElement getDomElement(String componentName) {
        DomElement domElement = domElements.get(componentName);

        // finally we will try to get from pool
        if (domElement == null) {
            LOG.info("Cannot get domElement '" + componentName + "' on page "
                    + pageName + "; try to find from the pool.");

            if (domElement == null) {
                domElement = WebUIManager.getInstance().getDomElementFromPool(
                        componentName);
            }

            LOG.info("Found domElement '" + componentName
                    + "' configuration on page " + domElement.getPageInfo()
                    + ". Use it for this page.");
        }

        return domElement;
    }

    // --------------------- GETTER / SETTER METHODS ---------------------

    public Map<String, DomElement> getDomElements() {
        return domElements;
    }

    public void setDomElements(Map<String, DomElement> domElements) {
        this.domElements = domElements;
    }

    public String getKeyDomElementName() {
        return keyDomElementName;
    }

    public void setKeyDomElementName(String keyDomElementName) {
        this.keyDomElementName = keyDomElementName;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrlExtension() {
        return urlExtension;
    }

    public void setUrlExtension(String urlExtension) {
        this.urlExtension = urlExtension;
    }

}
