package com.mitrais.guitestautomation.page;

public enum AsterxHtmlActionConditionEnum implements HtmlActionCondition {
	MEMBER_DROPDOWN_CONTENT("member_dropdown_content"), MEMBER_DROPDOWN_CONTAIN(
			"member_dropdown_contain"), MEMBER_MULTISELECT_GRID_CONTENT(
			"member_multiselect_grid_content"), MEMBER_MULTISELECT_GRID_CONTAIN(
			"member_multiselect_grid_contain"), MEMBER_MULTISELECT_LIST_CONTENT(
			"member_multiselect_list_content"), MEMBER_MULTISELECT_LIST_CONTAIN(
			"member_multiselect_list_contain"), MEMBER_MULTISELECT_SELECTED_CONTENT(
			"member_multiselect_selected_content"), MEMBER_MULTISELECT_SELECTED_CONTAINS(
			"member_multiselect_selected_contains"), MEMBER_MULTISELECT_OPTION_CONTENT(
			"member_multiselect_option_content"), MEMBER_MULTISELECT_OPTION_CONTAINS(
			"member_multiselect_option_contains"), APPOINTMENT_LOCATION_VALIDATION(
			"appointment_location_validation");

	private String value;

	AsterxHtmlActionConditionEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	/**
	 * Find a value in the list of enums. If no match is found, return the
	 * ENUM_NOT_FOUND value. Note that is is a perfect example of where optional
	 * objects would be a good fit.
	 */
	public static HtmlActionCondition findValue(String theValue) {
		AsterxHtmlActionConditionEnum[] values = values();

		for (AsterxHtmlActionConditionEnum htmlActionConditionEnum : values) {
			if (htmlActionConditionEnum.value.equals(theValue)) {
				return htmlActionConditionEnum;
			}
		}

		return GenericHtmlActionConditionEnum.ENUM_NOT_FOUND;
	}
}
