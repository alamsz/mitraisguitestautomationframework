package com.mitrais.guitestautomation.controller;

import static com.mitrais.guitestautomation.util.Browsers.Chrome;
import static com.mitrais.guitestautomation.util.Browsers.IE;
import static com.mitrais.guitestautomation.util.Browsers.Opera;
import static com.mitrais.guitestautomation.util.Browsers.Safari;
import static com.mitrais.guitestautomation.util.Constants.ACTION_PACE;
import static com.mitrais.guitestautomation.util.Constants.BASE_URL;
import static com.mitrais.guitestautomation.util.Constants.BROWSER;
import static com.mitrais.guitestautomation.util.Constants.BROWSER_SIZE;
import static com.mitrais.guitestautomation.util.Constants.BROWSER_VERSION;
import static com.mitrais.guitestautomation.util.Constants.CHROME_CONFIGURATION;
import static com.mitrais.guitestautomation.util.Constants.CUSTOM_CLASSPATH;
import static com.mitrais.guitestautomation.util.Constants.IE_CONFIGURATION;
import static com.mitrais.guitestautomation.util.Constants.OPERA_CONFIGURATION;
import static com.mitrais.guitestautomation.util.Constants.PAGES_DIRECTORY_NAME;
import static com.mitrais.guitestautomation.util.Constants.PAGE_LOAD_TIMEOUT;
import static com.mitrais.guitestautomation.util.Constants.PLATFORM;
import static com.mitrais.guitestautomation.util.Constants.PROJECT_NAME;
import static com.mitrais.guitestautomation.util.Constants.REMOTE_WEB_DRIVER_URL;
import static com.mitrais.guitestautomation.util.Constants.SAFARI_PADDING_LEFT;
import static com.mitrais.guitestautomation.util.Constants.SAFARI_PADDING_TOP;
import static com.mitrais.guitestautomation.util.Constants.SCREENSHOT_MODE;
import static com.mitrais.guitestautomation.util.Constants.SETTINGS_USE_XML;
import static com.mitrais.guitestautomation.util.Constants.USE_REMOTE;
import static com.mitrais.guitestautomation.util.Constants.DESKTOP_AUTOMATION_SERVER_PORT;
import static com.mitrais.guitestautomation.util.Constants.DESKTOP_SERVER_ENDPOINT_PATH;
import static com.mitrais.guitestautomation.util.Constants.DESKTOP_AUTOMATION_SCRIPT;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import Client.Robotil;

import com.google.common.base.Charsets;
import com.google.common.base.Optional;
import com.google.common.io.Resources;
import com.mitrais.guitestautomation.action.PageAction;
import com.mitrais.guitestautomation.page.DomElement;
import com.mitrais.guitestautomation.page.Page;
import com.mitrais.guitestautomation.util.Browsers;
import com.mitrais.guitestautomation.util.Constants;
import com.mitrais.guitestautomation.util.Drivers;
import com.mitrais.guitestautomation.util.ScreenshotMode;

/**
 * UIManager holds all pages and their web components, page actions. It loads
 * all configurations from settings.properties then go through all
 * registeredPages to load page object classes as well as their xml
 * configurations.
 */
public class WebUIManager {
    private static final Logger LOG = LoggerFactory
            .getLogger(WebUIManager.class);
    private static final String VERSION = "version";
    public static final String SYSTEM_BROWSERS_SETTING = "browsers";
    public static final String SYSTEM_REPORTS_PATH = "mitrais.reports";
    public static final String SYSTEM_CONFIGURATION_PATH = "mitrais.configs";
    public static final String SYSTEM_SCREENSHOT_PATH = "mitrais.screenshot";
    public static final String SYSTEM_TESTING_CONFIG_PATH = "test.configs";
    public static final String WEBDRIVER_CHROME_DRIVER = "webdriver.chrome.driver";
    public static final String WEBDRIVER_OPERA_DRIVER = "webdriver.chrome.driver";
    public static final String WEBDRIVER_IE_DRIVER = "webdriver.ie.driver";
    private static WebUIManager instance;

    private Map<String, Page> pages = new HashMap<>();
    private Map<String, DomElement> domElementPool = new HashMap<>();
    private Map<String, List<PageAction>> pageActions = new HashMap<>();

    // configurations and reportsPath
    private String configurationsPath;
    private String reportsPath;
    private String testConfig;

    // from mitrais.properties
    private String projectName = "";
    private boolean useRemoteWebDriver;
    private static String remoteWebDriverUrl;
    private static int pageLoadTimeout = 10000;
    private static int actionPace = 1000;
    private static int elementWaitTimeout = 3000;
    private static int elementPoolingTimeout = 250;
    private static int elementContentTimeout = 3000;
    private String customClasspath;
    private String baseUrl = "";
    private ScreenshotMode screenshotMode = ScreenshotMode.NONE;

    // from system properties
    private Browsers browser = null;
    private String platform = null;
    private String browserVersion = null;
    private Properties settings = new Properties();
    private Robotil remoteRobot = null;
    private int safariPaddingTop = 0;
    private int safariPaddingLeft = 0;
    private int desktopAutomationServerPort = 4723;
    private String desktopAutomationScriptPath = "";

    /**
     * Singleton instance of UIManager to hold information of pages and page
     * actions.
     *
     * @return The singleton UIManager
     *
     * @throws IOException
     */
    public static synchronized WebUIManager getInstance() {
        if (instance == null) {
            instance = new WebUIManager();
        }

        return instance;
    }

    /**
     * called by the UI to reintialize the driver code so subsequent invocations
     * will work.
     */
    public static void reinitialize() {
        instance = null;
        getInstance();
    }

    /*
     * private constructor.
     */
    private WebUIManager() {
        long startTime = System.currentTimeMillis();

        configurationsPath = System.getProperty(SYSTEM_CONFIGURATION_PATH);
        testConfig = System.getProperty(SYSTEM_TESTING_CONFIG_PATH);
        reportsPath = System.getProperty(SYSTEM_REPORTS_PATH);
        browser = getBrowserFromSystemProperty();

        String useRemote = System.getProperty("remote");

        useRemoteWebDriver = BooleanUtils.toBoolean(useRemote);

        File configurationsFile = StringUtils.isNotEmpty(configurationsPath) ? new File(
                configurationsPath) : new File("configurations");

        configurationsPath = configurationsFile.getAbsolutePath();
        getOrCreateLogsPath();
        LOG.info("Initializing ...");

        // read settings files:
        LOG.info("Reading mitrais.properties from [" + configurationsPath
                + File.separator + "test properties]");
        LOG.info("Accessing reports directory at [" + reportsPath + "]");

        try (InputStream stream = new FileInputStream(new File(testConfig))) {
            settings.load(stream);
            loadConfiguration();

            long endTime = System.currentTimeMillis();

            LOG.info("MITRAIS Gui Automation started successfully in ["
                    + (endTime - startTime) + "] milliseconds.");
        } catch (FileNotFoundException e) {
            LOG.error(
                    "An unexpected error has occurred; a configuration file could not be found.",
                    e);
        } catch (IOException e) {
            LOG.error("An unexpected error has occurred.", e);
        } catch (NullPointerException e) {
            LOG.error("An unexpected error has occurred. Null pointer detected");
        }
    }

    private Browsers getBrowserFromSystemProperty() {
        Optional<Browsers> possible = Browsers.findValueOf(System
                .getProperty(SYSTEM_BROWSERS_SETTING));

        if (possible.isPresent()) {
            return possible.get();
        }

        return Browsers.NOT_SPECIFIED;
    }

    private void getOrCreateLogsPath() {
        File logsFile;

        if (StringUtils.isNotEmpty(reportsPath)) {
            logsFile = new File(reportsPath);

            // recursively create file path if needed
            if (!logsFile.exists()) {
                logsFile.mkdirs();
            }
        } else {
            logsFile = new File("reports");
        }

        reportsPath = logsFile.getAbsolutePath();
    }

    // getter and setters
    public Map<String, List<PageAction>> getPageActions() {
        return pageActions;
    }

    public List<PageAction> getPageActionListFromPage(String pageName) {
        return pageActions.get(pageName);
    }

    public DomElement getDomElementFromPool(String identifier) {
        return domElementPool.get(identifier);
    }

    public ScreenshotMode getScreenshotMode() {
        return screenshotMode;
    }

    /**
     * Given a page name, return WebPage object in the Map of pages.
     *
     * @param pageName
     *            of the WebPage.
     *
     * @return WebPage.
     */
    public Page getPage(String pageName) {
        return pages.get(pageName);
    }

    public Robotil getRemoteRobot() {
        return remoteRobot;
    }

    public int getSafariPaddingTop() {
        return safariPaddingTop;
    }

    public int getSafariPaddingLeft() {
        return safariPaddingLeft;
    }

    public String getDesktopAutomationScriptPath() {
        return desktopAutomationScriptPath;
    }

    /**
     * loading setting.properties and populate all pages from its
     * configurations.
     *
     * @throws FileNotFoundException
     */
    private void loadConfiguration() throws IOException {
        if (StringUtils.isNotEmpty(settings.getProperty(PROJECT_NAME))) {
            projectName = settings.getProperty(PROJECT_NAME);
        }

        if (StringUtils.isNotEmpty(settings.getProperty(USE_REMOTE))) {
            useRemoteWebDriver = Boolean.valueOf(settings
                    .getProperty(USE_REMOTE));
        }

        if (useRemoteWebDriver) {
            if (StringUtils.isNotEmpty(settings
                    .getProperty(REMOTE_WEB_DRIVER_URL))) {
                remoteWebDriverUrl = settings
                        .getProperty(REMOTE_WEB_DRIVER_URL);
                URL url = new URL(remoteWebDriverUrl);
                String host = url.getHost();
                remoteRobot = new Robotil(host, 6666);
                safariPaddingTop = (StringUtils.isNotEmpty(settings
                        .getProperty(SAFARI_PADDING_TOP))) ? Integer
                        .parseInt(settings.getProperty(SAFARI_PADDING_TOP)) : 0;
                safariPaddingLeft = (StringUtils.isNotEmpty(settings
                        .getProperty(SAFARI_PADDING_LEFT))) ? Integer
                        .parseInt(settings.getProperty(SAFARI_PADDING_LEFT))
                        : 0;
                if (StringUtils.isNotEmpty(settings
                        .getProperty(DESKTOP_AUTOMATION_SERVER_PORT))) {
                    desktopAutomationServerPort = Integer.parseInt(settings
                            .getProperty(DESKTOP_AUTOMATION_SERVER_PORT));
                }

                if (StringUtils.isNotEmpty(settings
                        .getProperty(DESKTOP_AUTOMATION_SCRIPT))) {
                    desktopAutomationScriptPath = settings
                            .getProperty(DESKTOP_AUTOMATION_SCRIPT);
                }
            } else {
                LOG.error("When setting 'useRemote' to true, you must specify property: remoteWebDriverUrl.");
                Runtime.getRuntime().halt(0);
            }
        }

        boolean useXml = false;

        if (StringUtils.isNotEmpty(settings.getProperty(SETTINGS_USE_XML))) {
            useXml = Boolean.valueOf(settings.getProperty(SETTINGS_USE_XML));
        }

        String pagesDirectoryName = settings.getProperty(PAGES_DIRECTORY_NAME);

        // by default, use pages as the folder name
        pagesDirectoryName = (StringUtils.isEmpty(pagesDirectoryName)) ? "pages"
                : pagesDirectoryName;

        String pagesFolderPath = configurationsPath + File.separator
                + pagesDirectoryName;

        if (settings.getProperty(PAGE_LOAD_TIMEOUT) != null) {
            pageLoadTimeout = Integer.parseInt(settings.getProperty(
                    PAGE_LOAD_TIMEOUT).trim());
        }

        if (settings.getProperty(ACTION_PACE) != null) {
            actionPace = Integer.parseInt(settings.getProperty(ACTION_PACE)
                    .trim());
        }

        if (settings.getProperty(BROWSER) != null) {
            Optional<Browsers> possible = Browsers.findValueOf(settings
                    .getProperty(BROWSER));

            if (possible.isPresent()) {
                browser = possible.get(); // only override if it exists
            }
        }

        if (settings.getProperty(BASE_URL) != null) {
            baseUrl = settings.getProperty(BASE_URL);
        }

        if (settings.getProperty(SCREENSHOT_MODE) != null) {
            screenshotMode = ScreenshotMode.valueOf(settings
                    .getProperty(SCREENSHOT_MODE));
        }

        if (settings.getProperty(Constants.ELEMENT_WAIT_TIMEOUT) != null) {
            elementWaitTimeout = Integer.parseInt(settings.getProperty(
                    Constants.ELEMENT_WAIT_TIMEOUT).trim());
        }

        // Gather element pooling timeout from properties file
        if (settings.getProperty(Constants.ELEMENT_POOLING_TIMEOUT) != null) {
            elementPoolingTimeout = Integer.parseInt(settings.getProperty(
                    Constants.ELEMENT_POOLING_TIMEOUT).trim());
        }
        // Gather element content timeout from properties file
        if (settings.getProperty(Constants.ELEMENT_CONTENT_TIMEOUT) != null) {
            elementContentTimeout = Integer.parseInt(settings.getProperty(
                    Constants.ELEMENT_CONTENT_TIMEOUT).trim());
        }

        // Gather element content timeout from properties file
        if (settings.getProperty(Constants.BROWSER_VERSION) != null) {
            browserVersion = settings.getProperty(Constants.BROWSER_VERSION);
        }

        if (StringUtils.isNotBlank(settings.getProperty(CUSTOM_CLASSPATH))) {
            customClasspath = settings.getProperty(CUSTOM_CLASSPATH).trim();
        }

        // loop through directory, if useXml, add all files with xml postfix to
        // pages, otherwise add all json files
        File pageFolder = new File(pagesFolderPath);

        if (!pageFolder.isDirectory()) {
            LOG.error("'Pages' folder must be a directory, ["
                    + pageFolder.getAbsolutePath() + "] is invalid.");
            throw new RuntimeException('\'' + pageFolder.getAbsolutePath()
                    + "' must be a directory. ");
        }

        List<String> fileNames = Arrays.asList(pageFolder.list());

        // sort into alphabetical order so that we can always have override
        // features for web components
        Collections.sort(fileNames);
        LOG.info("Processing configuration files at " + configurationsPath
                + File.separator + "pages" + File.separator + ":");

        for (String fileName : fileNames) {
            addPage(useXml, pagesFolderPath, fileName);
        }
    }

    public static int getElementContentTimeout() {
        return elementContentTimeout;
    }

    public static void setElementContentTimeout(int elementContentTimeout) {
        WebUIManager.elementContentTimeout = elementContentTimeout;
    }

    /** add page entry with DOM elements. */
    private void addPage(boolean useXml, String pagesFolderPath, String fileName)
            throws IOException {
        try (FileInputStream fileIn = new FileInputStream(pagesFolderPath
                + File.separator + fileName)) {
            LOG.info("Processing page configuration file: " + fileName
                    + ", useXml = " + useXml);

            Page page;

            if (useXml) {
                if (!fileName.toLowerCase().endsWith(".xml")) {
                    LOG.info("Ignored file: " + fileName);

                    return;
                }

                try {
                    JAXBContext jaxbContext = JAXBContext
                            .newInstance(Page.class);
                    Unmarshaller jaxbUnmarshaller = jaxbContext
                            .createUnmarshaller();

                    page = (Page) jaxbUnmarshaller.unmarshal(fileIn);
                    LOG.info("Loaded web page " + page.getPageName()
                            + " from  " + fileName);
                } catch (JAXBException e) { // error out if we cannot marshall
                                            // the XML into a page
                    LOG.error(e.getMessage());

                    return;
                }
            } else {
                if (!fileName.toLowerCase().endsWith(".json")) {
                    return;
                }

                try {
                    ObjectMapper objectMapper = new ObjectMapper();

                    page = objectMapper.readValue(fileIn, Page.class);
                } catch (IOException e) {
                    LOG.warn("Error parsing page [" + fileName + ']', e);

                    return;
                }
            }

            // something is wrong with the page, let's ignore it.
            if (page == null) {
                LOG.warn("Failed to load web page [" + fileName
                        + "] continue to load next.");

                return;
            }

            // put the pages into internal storage!
            pages.put(page.getPageName(), page);

            // add to domElementPool
            Map<String, DomElement> domElementsInPage = page.getDomElements();

            for (String componentIdentifier : domElementsInPage.keySet()) {
                DomElement domElement = domElementsInPage
                        .get(componentIdentifier);

                domElement.setIdentifier(componentIdentifier);

                // only add the first one in so that other page can override it.
                if (domElementPool.get(componentIdentifier) == null) {
                    domElement.setPageInfo(page.getPageName());
                    domElementPool.put(componentIdentifier, domElement);
                }
            }

            LOG.info("Successfully adding page entry of " + page.getPageName()
                    + " with domElements: " + page.getDomElements());
        }
    }

    // chrome driver
    public WebDriver getChromeDriver() throws MalformedURLException {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();

        capabilities.setCapability(
                "chrome.switches",
                Arrays.asList(settings.getProperty(CHROME_CONFIGURATION
                        + "chrome.switches")));

        WebDriver driver;

        if (useRemoteWebDriver) {
            driver = new RemoteWebDriver(new URL(remoteWebDriverUrl),
                    capabilities);
        } else {
            if (System.getProperty(WEBDRIVER_CHROME_DRIVER) == null) {
                String chromeDriver = null;
                if (settings.getProperty(CHROME_CONFIGURATION
                        + WEBDRIVER_CHROME_DRIVER) == null) {
                    chromeDriver = Drivers.CHROME.getDriverName();
                } else {
                    chromeDriver = settings.getProperty(CHROME_CONFIGURATION
                            + WEBDRIVER_CHROME_DRIVER);
                }

                if (StringUtils.isEmpty(chromeDriver)) {
                    throw new IllegalArgumentException(
                            "No chrome driver specified! Please specify as a system property or in your Mitrais.properties file.");
                }

                System.setProperty(WEBDRIVER_CHROME_DRIVER, chromeDriver);
            }

            driver = new ChromeDriver(capabilities);
        }

        return driver;
    }

    // jsheridan CODEREVIEW - note that if we used an enum, this logic could be
    // moved into that, so the browser enum knew how to get it's driver
    // firefox
    public WebDriver getFirefoxDriver() throws MalformedURLException {
        WebDriver driver;
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(VERSION, applyVersion());
        FirefoxProfile profile = new FirefoxProfile();
        String proxyType = settings.getProperty("firefox.network.proxy.type");

        if (proxyType.equalsIgnoreCase(ProxyType.AUTODETECT.name())) {
            profile.setPreference("network.proxy.type",
                    ProxyType.AUTODETECT.ordinal());
        } else if (proxyType.equalsIgnoreCase(ProxyType.DIRECT.name())) {
            profile.setPreference("network.proxy.type",
                    ProxyType.DIRECT.ordinal());
        }
        capabilities.setCapability(FirefoxDriver.PROFILE, profile);
        if (useRemoteWebDriver) {

            driver = new RemoteWebDriver(new URL(remoteWebDriverUrl),
                    capabilities);
        } else {

            driver = new FirefoxDriver(null, profile, capabilities);
        }

        return driver;
    }

    // ie driver
    public WebDriver getIEDriver() throws MalformedURLException {
        WebDriver driver;
        // set the capabilities of Internet Explorer to make sure browser ready
        // for test
        DesiredCapabilities capabilitiesInternet = new DesiredCapabilities();
        for (Object keys : settings.keySet()) {
            String keyString = String.valueOf(keys);
            if (keyString.startsWith(IE_CONFIGURATION)
                    && !keyString
                            .equals(IE_CONFIGURATION + WEBDRIVER_IE_DRIVER)) {
                String capability = keyString.substring(IE_CONFIGURATION
                        .length());
                boolean value = Boolean.valueOf((String) settings.get(keys));
                capabilitiesInternet.setCapability(capability, value);
            }
        }
        capabilitiesInternet.setCapability("enablePersistentHover", false);
        capabilitiesInternet.setCapability("nativeEvents", true);
        capabilitiesInternet.setCapability("requireWindowFocus", true);
        if (useRemoteWebDriver) {
            driver = new RemoteWebDriver(new URL(remoteWebDriverUrl),
                    capabilitiesInternet);
        } else {
            if (System.getProperty(WEBDRIVER_IE_DRIVER) == null) {
                String ieDriver = null;
                if (settings
                        .getProperty(IE_CONFIGURATION + WEBDRIVER_IE_DRIVER) == null) {
                    ieDriver = Drivers.CHROME.getDriverName();
                } else {
                    ieDriver = settings.getProperty(IE_CONFIGURATION
                            + WEBDRIVER_IE_DRIVER);
                }

                if (StringUtils.isEmpty(ieDriver)) {
                    throw new IllegalArgumentException(
                            "No IE driver specified! Please specify as a system property or in your Mitrais.properties file.");
                }

                System.setProperty(WEBDRIVER_IE_DRIVER, ieDriver);
            }
            driver = new InternetExplorerDriver(capabilitiesInternet);
        }
        return driver;
    }

    // safari
    public WebDriver getSafariDriver() throws MalformedURLException {
        if (useRemoteWebDriver) {
            DesiredCapabilities capabilities = DesiredCapabilities.safari();
            //
            // capabilities.setCapability(PLATFORM, applyPlatform());
            // capabilities.setCapability(VERSION, applyVersion());

            return new RemoteWebDriver(new URL(remoteWebDriverUrl),
                    capabilities);
        } else {
            return new SafariDriver();
        }
    }

    // safari
    public WebDriver getIPadDriver() throws MalformedURLException {
        if (useRemoteWebDriver) {
            DesiredCapabilities capabilities = DesiredCapabilities.ipad();

            capabilities.setCapability(PLATFORM, applyPlatform());
            capabilities.setCapability(VERSION, applyVersion());

            return new RemoteWebDriver(new URL(remoteWebDriverUrl),
                    capabilities);
        } else {
            return new SafariDriver();
        }
    }

    public WebDriver getOperaDriver() throws MalformedURLException {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();

        capabilities.setCapability(
                "chrome.switches",
                Arrays.asList(settings.getProperty(OPERA_CONFIGURATION
                        + "chrome.switches")));
        WebDriver driver;

        if (useRemoteWebDriver) {
            driver = new RemoteWebDriver(new URL(remoteWebDriverUrl),
                    capabilities);
        } else {
            if (System.getProperty(WEBDRIVER_OPERA_DRIVER) == null) {
                String chromeDriver = null;
                if (settings.getProperty(OPERA_CONFIGURATION
                        + WEBDRIVER_OPERA_DRIVER) == null) {
                    chromeDriver = Drivers.CHROME.getDriverName();
                } else {
                    chromeDriver = settings.getProperty(OPERA_CONFIGURATION
                            + WEBDRIVER_OPERA_DRIVER);
                }

                if (StringUtils.isEmpty(chromeDriver)) {
                    throw new IllegalArgumentException(
                            "No chrome driver specified! Please specify as a system property or in your Mitrais.properties file.");
                }

                System.setProperty(WEBDRIVER_OPERA_DRIVER, chromeDriver);
            }

            driver = new ChromeDriver(capabilities);
        }
        return driver;
    }

    public JavascriptExecutor getJQueryDriver() {
        return (JavascriptExecutor) getWebDriver();
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    private WebDriver driver;
    private WebDriver autoitDriver;
    private WebDriver screenshotDriver;

    public WebDriver getWebDriver() {
        if (driver == null) {
            try {
                if (browser == Chrome) {
                    driver = getChromeDriver();
                } else if (browser == IE) {
                    driver = getIEDriver();
                } else if (browser == Safari) {
                    driver = getSafariDriver();
                } else if (browser == Opera) {
                    driver = getOperaDriver();
                } else {
                    driver = getFirefoxDriver();
                }
                driver.get(baseUrl);
                if (settings.get(BROWSER_SIZE) == null) {
                    driver.manage().window().maximize();
                } else {
                    String[] browserSize = settings.get(BROWSER_SIZE)
                            .toString().split(" ");
                    int width = Integer.valueOf(browserSize[0]);
                    int height = Integer.valueOf(browserSize[1]);
                    driver.manage().window()
                            .setSize(new Dimension(width, height));
                    driver.manage().window().setPosition(new Point(0, 0));
                }
                if (useRemoteWebDriver) {
                    screenshotDriver = new Augmenter().augment(driver);
                } else {
                    screenshotDriver = driver;
                }
            } catch (MalformedURLException e) {
                driver = null;
                LOG.error("Malform url at get web driver", e);
            }
        }
        return driver;
    }

    public boolean setupAutoItDriver() throws MalformedURLException {
        if (autoitDriver == null && useRemoteWebDriver) {
            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
            desiredCapabilities.setCapability("browserName", "AutoIt");
            String autoItServerAddress = "http://"
                    + (new URL(remoteWebDriverUrl).getHost()) + ":"
                    + desktopAutomationServerPort
                    + DESKTOP_SERVER_ENDPOINT_PATH;
            autoitDriver = new RemoteWebDriver(new URL(autoItServerAddress),
                    desiredCapabilities);
        }
        return autoitDriver != null;
    }

    public WebDriver getAutoitDriver() {
        return autoitDriver;
    }

    public WebDriver getScreenshotDriver() {
        return screenshotDriver;
    }

    public void terminateWebDriver() {
        if (driver != null) {
            // driver.close();
            driver.quit();
        }
        if (autoitDriver != null) {
            autoitDriver.quit();
        }
        driver = null;
        autoitDriver = null;
    }

    public static void loadJQuery(JavascriptExecutor jsDriver) {
        Object jquery = jsDriver
                .executeScript(" if ( typeof $ != 'undefined') { return 1;} else { return null; }");

        if (jquery == null) {
            URL jqueryUrl = Resources.getResource("jquery-1.8.0.min.js");
            String jqueryText = "";

            try {
                jqueryText = Resources.toString(jqueryUrl, Charsets.UTF_8);
            } catch (IOException e) {
                LOG.warn("Error obtaining jquery library.", e);
            }

            LOG.info("\tEnable Jquery");
            jsDriver.executeScript(jqueryText);
        }
    }

    public String getResource(String jsName) {
        URL jsUrl = Resources.getResource(jsName);
        String jsText = "";

        try {
            jsText = Resources.toString(jsUrl, Charsets.UTF_8);
        } catch (IOException e) {
            LOG.info("Error in javascript!", e);
        }

        return jsText;
    }

    public String getResourceSingleLine(String jsName) {
        URL jsUrl = Resources.getResource(jsName);
        String jsText = "";

        try {
            jsText = Resources.toString(jsUrl, Charsets.UTF_8)
                    .replace("\n", "").replace("\r", "");
        } catch (IOException e) {
            LOG.info("Error in javascript!", e);
        }

        return jsText;
    }

    private String applyPlatform() {
        if ((platform == null) || platform.trim().isEmpty()) {
            return settings.getProperty(PLATFORM);
        } else {
            return platform;
        }
    }

    private String applyVersion() {
        if ((browserVersion == null) || browserVersion.trim().isEmpty()) {
            return settings.getProperty(BROWSER_VERSION);
        } else {
            return browserVersion;
        }
    }

    public String getConfigurationsPath() {
        return configurationsPath;
    }

    public String getLogsPath() {
        return reportsPath;
    }

    public void setLogsPath(String logsPath) {
        reportsPath = logsPath;
    }

    public static String getRemoteWebDriverUrl() {
        return remoteWebDriverUrl;
    }

    public boolean isUseRemoteWebDriver() {
        return useRemoteWebDriver;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public static int getPageLoadTimeout() {
        return pageLoadTimeout;
    }

    public static void setPageLoadTimeout(int pageLoadTimeout) {
        WebUIManager.pageLoadTimeout = pageLoadTimeout;
    }

    public static int getActionPace() {
        return actionPace;
    }

    public static void setActionPace(int actionPace) {
        WebUIManager.actionPace = actionPace;
    }

    public void setUseRemoteWebDriver(boolean useRemoteWebDriver) {
        this.useRemoteWebDriver = useRemoteWebDriver;
    }

    public Browsers getBrowser() {
        return browser;
    }

    public void setBrowser(Browsers browser, boolean overwriteSystem) {
        // if system property is set, use that instead
        if (overwriteSystem) {
            this.browser = browser;
        } else {
            this.browser = getBrowserFromSystemProperty();
        }
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getCustomClasspath() {
        return customClasspath;
    }

    public void setCustomClasspath(String customClasspath) {
        this.customClasspath = customClasspath;
    }

    public Map<String, Page> getPages() {
        return pages;
    }

    public static int getElementWaitTimeout() {
        return elementWaitTimeout;
    }

    public static int getElementPoolingTimeout() {
        return elementPoolingTimeout;
    }
}
