var cellRect = arguments[0].getBoundingClientRect();
var appointmentRect = arguments[1].getBoundingClientRect();
return (cellRect.top <= appointmentRect.top && cellRect.left <= appointmentRect.left && cellRect.bottom >= appointmentRect.bottom && cellRect.right >= appointmentRect.right)